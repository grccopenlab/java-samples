package edu.grcc.directory_sample;

/**
 * This class defines the attributes of a Person for our directory program.
 * This file should accompany Example02.java
 * 
 * @author Nathaniel Knight
 * @version 1
 * @since 2015-09-30
 *
 */
public class Person {
	public String name;
	public String relationship;
	
	/**
	 * Constructs a new Person using a string for name and another string
	 * for relationship.
	 * 
	 * @param	name			the name of the person
	 * @param	relationship	the relationship of the person to the user
	 */
	Person(String name, String relationship)
	{
		this.name = name;
		this.relationship = relationship;
	}
	
}
