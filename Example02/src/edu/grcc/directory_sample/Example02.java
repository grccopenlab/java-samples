package edu.grcc.directory_sample;

import java.util.Scanner;

/**
 * This is the second pass on a business directory project.
 * 
 * This second pass adds a {@link Person} class to the project which is used
 * to define the attributes of a person, and we now ask the user for information
 * about two people to demonstrate the difference between instances of an
 * Class.
 * 
 * We also add a prompt({@linkplain String} prompt, {@linkplain Scanner} input) method
 * to the main program class, so we can make our primary code slightly cleaner, and to
 * demonstrate the use of methods to combine routine tasks.
 * 
 * @author Nathaniel Knight
 * @version 2
 * @since 2015-09-30
 *
 */
public class Example02
{
	/**
	 * Asks for two names and relationships, then outputs the Person
	 * defined by each name + relationship pair to the user.
	 * 
	 * @param	args	unused at present
	 */
	public static void main(String[] args)
	{
		// We set up our necessary input and variables here.
		Scanner input = new Scanner(System.in);
		String name;
		String relationship;
		
		// The following lines create two new instances of our Person class
		Person person1;
		Person person2;
		
		// Get and create person1
		name = prompt("Please enter the name of a person: ", input);
		relationship = prompt("Is this person a customer, employee, or supplier? ", input);
		person1 = new Person(name, relationship);
		// Get and create person2
		name = prompt("Please enter the name of another person: ", input);
		relationship = prompt("Is this person a customer, employee, or supplier? ", input);
		person2 = new Person(name, relationship);

		System.out.println("");
		
		// Output each person in order
		System.out.println(person1.name + " is one of your " + person1.relationship + "s.");
		System.out.println(person2.name + " is one of your " + person2.relationship + "s.");
		
		input.close();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		// Replaces our two-line prompt construction with a custom method. 
		System.out.print(prompt);
		return input.nextLine();
	}

}
