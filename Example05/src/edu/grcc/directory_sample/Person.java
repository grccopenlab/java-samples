package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This class defines the attributes of a Person for our directory program.
 * This file should accompany Example05.java
 * 
 * @author Nathaniel Knight
 * @version 4
 * @since 2015-09-30
 *
 */
public class Person {
	private String name;
	private String relationship;
	
	/**
	 * Constructs a new Person using a string for name and another string
	 * for relationship.
	 * 
	 * @param	name	the name of the person
	 * @param	relationship	the relationship of the person to the user
	 */
	Person(String name, String relationship)
	{
		setName(name);
		setRelationship(relationship);
	}
	
	/**
	 * Constructs a Person from an existing Person, copying the name
	 * and relationship attributes from the instance passed in.
	 * 
	 * @param	basePerson	the Person being used as a base
	 */
	Person(Person person)
	{
		this.name = person.getName();
		this.relationship = person.getRelationship();
	}

	/**
	 * 
	 * @return	the name of this object
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name value of this object
	 * 
	 * @param	name	the value to assign to name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return		the relationship value of this object
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * Set the relationship value of this object
	 * 
	 * @param	relationship	the value to assign to relationship
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	/**
	 * A builder method to collect the information needed to create a new
	 * instance of Person. Includes internal error checking of the user's
	 * entered data.
	 * 
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a new Person to be used elsewhere in the program
	 */
	public static Person newPerson(Scanner input)
	{
		String name, relationship;
		boolean valid = false;
		
		do
		{
			name = Example05.prompt("Please enter the name of a person: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			relationship = Example05.prompt("Is this person a customer, employee, or supplier? ", input);
			if(relationship.equalsIgnoreCase("customer") || relationship.equalsIgnoreCase("employee") || relationship.equalsIgnoreCase("supplier"))
			{
				valid = true;
			}
			else				
			{
				System.out.println("Invalid name.");
			}
		} while (!valid);
		
		Person person = new Person(name, relationship);
		return person;
	}

	/**
	 * Validates a Person created through any method, particularly those
	 * created without the {@linkplain newPerson} builder method
	 * 
	 * @return		a {@linkplain Boolean} representing if the name or relationship
	 * values of this object are valid
	 */
	public boolean isValidPerson()
	{
		boolean valid = true;

		if(name == null || name.equals(""))
		{
			valid = false;
			System.out.println("Invalid name.");
		}
		if(!relationship.toLowerCase().equals("customer") &&
				!relationship.toLowerCase().equals("employee") &&
				!relationship.toLowerCase().equals("supplier"))
		{
			valid = false;
			System.out.println("Invalid relationship.");
		}
		
		return valid;
	}
}
