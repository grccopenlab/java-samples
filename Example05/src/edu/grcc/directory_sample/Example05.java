package edu.grcc.directory_sample;
import java.util.ArrayList;
import java.util.Scanner;

public class Example05 {
	public static final char QUIT = 'q';

	/**
	 * This is the fifth pass on a business directory project.
	 * 
	 * In this version, we add a main program loop and menu to control the program
	 * flow. Users can add, remove, or list people, and can quit by entering 'q' at
	 * the main menu. We also replace the linear two-person data entry with an
	 * ArrayList so that the user can enter as many people as they like.
	 * 
	 *  
	 * @author Nathaniel Knight
	 * @version 5
	 * @since 2015-09-30
	 * @modified 2015-11-03
	 *
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String option = "";
		ArrayList<Person> people = new ArrayList<Person>(10);
		boolean valid = false;
		
		/*
		 * A main program loop allows a program to run until the user is finished
		 * with it, and tells the program to close. This can bee a good place to
		 * use a do-while loop, wince we will be using the user's input to decide
		 * where we go in the program, including the choice to go out of it.
		 */
		do	// start main program loop
		{
			/*
			 * A menu loop is a lot like a main program loop -- it is driven by
			 * user input to determine what we will do. An invalid entry should
			 * result in the user being prompted for a new entry until they give
			 * the program a valid choice.
			 */
			do // start menu loop
			{
				valid = false;	//	we make sure that valid is always false when the menu starts
				showMenu();	//	we show the menu
				option = input.nextLine();	//	we get the user's input
				if(compareFirstTo(option, 'q', false))
				{ System.exit(0);; }
				if(!valid && (Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3))
				{ valid = true; }
			} while(!valid);	// end menu loop
			
			if(Integer.valueOf(option) == 1)	// Menu Option 1: Add Person
			{
				people.add(Person.newPerson(input));
				System.out.println("Added " + people.get(people.size()-1).getName() + ".");
			}
			
			if(Integer.valueOf(option) == 2)	// Menu Option 2: Delete Last Person
			{
				System.out.println("Removing " + people.get(people.size()-1).getName() + ".");
				people.remove(people.size()-1);
			}
			
			if(Integer.valueOf(option) == 3)	// Menu Option 3: Review All People
			{
				System.out.println("Listing all people:");
				for(int i=0; i<people.size(); i++)
				{
					System.out.println(people.get(i).getName() + " is one of your " + people.get(i).getRelationship() + "s.");
				}
				System.out.println("");
			}
		} while(option.toLowerCase().charAt(0) != QUIT);	// end main program loop
				
		input.close();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
	
	/**
	 * Takes in a {@linkplain String} and a char, and compares the first character of
	 * the string to the char. Also takes a {@linkplain Boolean} to determine if this
	 * comparison is case sensitive or not. Note that the char must be a non-uppercase
	 * letter for case-insensitive comparisons.
	 * 
	 * @param string
	 * @param c
	 * @param caseSensitive
	 * @return
	 */
	public static boolean compareFirstTo(String string, char c, boolean caseSensitive)
	{
		if(caseSensitive) { return string.charAt(0) == c; }
		return string.toLowerCase().charAt(0) == c;
	}
	
	/**
	 * Shows the main menu for the program.
	 */
	public static void showMenu()
	{
		System.out.println("Please select an option below:");
		System.out.println("1. Add a person");
		System.out.println("2. Remove the last person");
		System.out.println("3. Review all people");
		System.out.println("Q. Quit this program");
	}
}
