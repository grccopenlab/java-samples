import java.util.ArrayList;
import java.util.Scanner;

public class ScoreCalc
{
	public static void main(String[] args)
	{
		ArrayList<Integer> scores = new ArrayList<Integer>();
		Scanner input = new Scanner(System.in);
		String sizeEntry;
		int size = 0;
		
		do
		{
			do
			{
				System.out.print("Please enter the number of scores (1-100, or 'q' to quit): ");
				sizeEntry = input.next();
				if(sizeEntry.toLowerCase().charAt(0) == 'q') { quit(input); }
				else { size = Integer.valueOf(sizeEntry); }
			} while(size <= 0);
			
			for(int i = 0; i < size; i++)
			{
				scores.add(getScore("Please enter score " + (i+1) + ": ", input));
			}
			scores.trimToSize();
			
			System.out.println("You entered " + size + " scores.");
			
			getHighScore(scores);
			getLowScore(scores);
			getAverageScore(scores);
			
		}while(true);
	}

	private static void quit(Scanner input) {
		input.close();
		System.exit(0);
	}

	private static void getAverageScore(ArrayList<Integer> scores)
	{
		double score = 0d;
		for(int i = 0; i < scores.size(); i++) { score += ((double)scores.get(i)); }
		score = score / (double)scores.size();
		System.out.println("The average score was " + score + ".");
	}

	private static void getLowScore(ArrayList<Integer> scores)
	{
		int score = Integer.MAX_VALUE;
		for(int i = 0; i < scores.size(); i++)
		{
			if(scores.get(i) < score) { score = scores.get(i); }
		}
		System.out.println("The lowest score was " + score + ".");
	}

	private static void getHighScore(ArrayList<Integer> scores)
	{
		int score = 0;
		for(int i = 0; i < scores.size(); i++)
		{
			if(scores.get(i) > score) { score = scores.get(i); }
		}
		System.out.println("The highest score was " + score + ".");
	}

	private static int getScore(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return Integer.valueOf(input.nextInt());
	}
}