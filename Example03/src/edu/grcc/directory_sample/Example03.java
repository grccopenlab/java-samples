package edu.grcc.directory_sample;

import java.util.Scanner;

/**
 * This is the third pass on a business directory project.
 * 
 * This pass adds security to the Person class with getters and setters, plus a static
 * method in Person which gets the information to make a new Person, and returns that
 * Person into the main program. Other than retrieving names and relationships from
 * the getter, Example03.java is almost identical to Example02.java
 *  
 * @author Nathaniel Knight
 * @version 3
 * @since 2015-09-30
 *
 */
public class Example03
{
	/**
	 * Builds two people, then outputs their names and relationships on separate
	 * lines.
	 * 
	 * @param	args	unused at present
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Person person1;
		Person person2;
		
		// Note that we don't use the prompt() method directly in this section now:
		// instead we use the static method newPerson(Scanner input) from our
		// updated Person.java file.
		person1 = Person.newPerson(input);
		person2 = Person.newPerson(input);
		System.out.println("");
		
		// Now we use getters to fill in the values for the name and relationship
		System.out.println(person1.getName() + " is one of your " + person1.getRelationship() + "s.");
		System.out.println(person2.getName() + " is one of your " + person2.getRelationship() + "s.");
		
		input.close();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the
	 * program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
}
