package edu.grcc.directory_sample;

import java.util.Scanner;

/**
 * This class defines the attributes of a Person for our directory program.
 * This file should accompany Example03.java
 * 
 * @author Nathaniel Knight
 * @version 2
 * @since 2015-09-30
 *
 */
public class Person {
	// We are now limiting access to the actual values of name and
	// relationship to only functions inside of the Person class.
	// This is encapsulation, and prevents other part of our
	// program from altering the values accidentally.
	private String name;
	private String relationship;
	
	/**
	 * Constructs a new Person using a string for name and another string
	 * for relationship.
	 * 
	 * @param	name			the name of the person
	 * @param	relationship	the relationship of the person to the user
	 */
	Person(String name, String relationship)
	{
		this.name = name;
		this.relationship = relationship;
	}
	
	/**
	 * Constructs a Person from an existing Person, copying the name
	 * and relationship attributes from the instance passed in.
	 * 
	 * @param	basePerson	the Person being used as a base
	 */
	// A copy constructor allows us to use the = symbol to assign an
	// existing Person's attributes to a newly created Person, which
	// may exist in a broader scope than the existing Person
	Person(Person basePerson)
	{
		this.name = basePerson.getName();
		this.relationship = basePerson.getRelationship();
	}

	/**
	 * 
	 * @return	the name of this object
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name value of this object
	 * 
	 * @param	name	the value to assign to name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return	the relationship value of this object
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * Set the relationship value of this object
	 * 
	 * @param	relationship	the value to assign to relationship
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	/**
	 * A builder method to collect the information needed to create a new
	 * instance of Person
	 * 
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a new Person to be used elsewhere in the program
	 */
	public static Person newPerson(Scanner input)
	{
		String name = Example03.prompt("Please enter the name of a person: ", input);
		String relationship = Example03.prompt("Is this person a customer, employee, or supplier? ", input);
		Person person = new Person(name, relationship);
		return person;
	}
}
