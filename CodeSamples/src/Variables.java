import java.util.Scanner;

public class Variables {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		int a, b, c;
		
		System.out.println("This demonstrates variables and arithmetic.");
		
		System.out.print("Please enter a number: ");
		a = Integer.valueOf(input.nextInt());
		System.out.print("Please enter a second number: ");
		b = Integer.valueOf(input.nextInt());
		System.out.print("Please enter a third number: ");
		c = Integer.valueOf(input.nextInt());
		
		System.out.println("");
		System.out.println("Java can add, subtract, multiply, divide, and calculate remainders for division.");
		System.out.println("You can add variables into strings for output.");
		int d = a + 5;
		System.out.println("Your first number is " + a + ". " + a + " plus 5 equals " + d + ".");
		
		System.out.println("");
		System.out.println("You can also do math in the middle of output, if you use parentheses.");
		System.out.println("Your second number, " + b + ", minus 3 equals " + (b-3) + ".");
		
		System.out.println("");
		System.out.println("Division will maintain data types given to it.");
		System.out.println("7 divided by 4 equals " + (7/4) + ", with a remainder of " + (7%4) + ".");
		
		System.out.println("");
		System.out.println("Multiplication is also a basic operation.");
		d = a + b; // This line changes the value of d from behind the scenes.
		System.out.println("The sum of " + a + " and " + b + " is " + d + ".");
		System.out.println("The product of " + c + " and " + d + " is " + (c*d) + ".");

	}

}
