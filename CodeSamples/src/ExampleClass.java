import java.util.Scanner;

/**
 * 
 * @author Nathaniel Knight (nathaniel.d.knight@gmail.com)
 *
 */
public class ExampleClass {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Square s1 = new Square();
		Square s2 = new Square();
		
		System.out.print("Please enter the length of square one: ");
		s1.setLength(Integer.valueOf(input.nextInt()));
		System.out.println("The area of square one is " + s1.area() + " square units.");
		
		System.out.print("Please enter the scale of square two: ");
		s2.scale(Integer.valueOf(input.nextInt()));
		System.out.println("The area of square two is " + s2.area() + " square units.");
		
		System.out.println("Both squares have a combined area of " + (s1.area() + s2.area()) + " square units.");
		
		input.close();
	}

}

class Square
{
	// A square can be stored in one variable -- length -- but it may have other properties
	private int length;
	
	// The default constructor makes a unit square
	public Square()
	{ setLength(1); }
	
	// Standard setter method
	/**
	 * 
	 * @param i = the length of a side
	 */
	public void setLength(int i)
	{ length = i; }
	
	// Standard getter method
	public int getLength()
	{ return length; }
	
	// A read-only field, we can't set area, but we calculate it by squaring length
	public int area()
	{ return (getLength() * getLength()); }
	
	// A write-only field, allows us to increase the size of each square
	public void scale(int s)
	{ setLength((getLength() * s)); }
}