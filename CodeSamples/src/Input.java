import java.util.Scanner;

public class Input
{
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		String userName;
		
		System.out.print("Please enter your name, user: ");
		userName = input.next();
		System.out.println("Hello " + userName);
	}
}
