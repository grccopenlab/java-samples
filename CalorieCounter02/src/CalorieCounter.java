import java.util.Scanner; // This package is required for input through Scanner

// This is the second prototype CalorieCounter program -- it will use
// methods and a custom class to achieve its goal of calculating how
// many calories a user has eaten over the course of one day with four
// meals. Any meal skipped can be entered as 0 for all values.

public class CalorieCounter {

	public static void main(String args[]) {
		// This is where we declare all of our variables for the program.
		String userName;
		// This is required to get input
		Scanner input = new Scanner(System.in);
		// our other input variables will now be instances of the MealData
		// class,  a custom class we can define properties and methods for
		MealData breakfast = new MealData("breakfast");
		MealData lunch = new MealData("lunch");
		MealData supper = new MealData("supper");
		MealData snack = new MealData("snack");
		// But we will keep total values for each nutrient as well:
		int caloriesFat, caloriesPrtn, caloriesCarb;

		// This is where we will begin our input collection code.
		System.out.print("Please enter your name: ");
		userName = input.nextLine();
		
		programInfo(); // this displays the program information for the user
		
		// Here we get per-meal input. Our getMealData method will need to know
		// the userName, which mealData object we're filling in, and a scanner
		
		// Breakfast
		getMealData(userName, breakfast, input);
		
		// Lunch
		getMealData(userName, lunch, input);
		
		// Supper
		getMealData(userName, supper, input);

		// Snack
		getMealData(userName, snack, input);
		// End of Meal Data Collection
		// End of Data Collection
		
		// In this section, we perform our calculations using the data entered above:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Calculating fat calories...");
		caloriesFat = (breakfast.gramsFat + lunch.gramsFat + supper.gramsFat + snack.gramsFat) * MealData.FatCalories;
		System.out.println("Calculating protein calories...");
		caloriesPrtn = (breakfast.gramsPrtn + lunch.gramsPrtn + supper.gramsPrtn + snack.gramsPrtn) * MealData.PrtnCalories;
		System.out.println("Calculating carbohydrate calories...");
		caloriesCarb = (breakfast.gramsCarb + lunch.gramsCarb + supper.gramsCarb + snack.gramsCarb) * MealData.CarbCalories;
		System.out.println("Calculations complete.");
		
		// In this section, we use our calculated values to output totals:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Your calorie report is ready, " + userName + "!");
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("At breakfast you had a total of " + breakfast.mealCalories() + " calories.");
		System.out.println("At lunch you had a total of " + lunch.mealCalories() + " calories.");
		System.out.println("At supper you had a total of " + supper.mealCalories() + " calories.");
		System.out.println("At snack you had a total of " + snack.mealCalories() + " calories.");
		
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("You had " + (breakfast.gramsFat + lunch.gramsFat + supper.gramsFat + snack.gramsFat) + "g of fat, for a total of " + caloriesFat + " calories from fat.");
		System.out.println("You had " + (breakfast.gramsPrtn + lunch.gramsPrtn + supper.gramsPrtn + snack.gramsPrtn) + "g of protein, for a total of " + caloriesPrtn + " calories from protein.");
		System.out.println("You had " + (breakfast.gramsCarb + lunch.gramsCarb + supper.gramsCarb + snack.gramsCarb) + "g of carbohydrates, for a total of " + caloriesCarb + " calories from carbohydrates.");
		
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("You had a total of " + (breakfast.mealCalories() + lunch.mealCalories() + supper.mealCalories() + snack.mealCalories()) + " calories for the day." );
		
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Thank you for using the calorie calculator.");
		
		input.close();		
	}
	
	public static void getMealData(String name, MealData meal, Scanner in)
	{
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Information for " + meal.mealName);
		System.out.println(name + " please enter the grams of fat you had with " + meal.mealName + ": ");
		meal.gramsFat = in.nextInt();
		System.out.println(name + " please enter the grams of protein you had with " + meal.mealName + ": ");
		meal.gramsPrtn = in.nextInt();
		System.out.println(name + " please enter the grams of carbohydrates you had with " + meal.mealName + ": ");
		meal.gramsCarb = in.nextInt();
	}
	
	public static void programInfo()
	{
		// Here is a text about the program:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("This program is designed to take your eating habits and calculate the number ");
		System.out.println("of calories you took in for a day. Please enter the numbers below in grams, "); 
		System.out.println("not ounces, eaten at the listed meal. If you did not have the listed nutrient ");
		System.out.println("at the listed meal, enter 0. If you did not eat the listed meal, enter 0 for ");
		System.out.println("all nutrients.");
		System.out.println(""); // this blank line provides padding for the user between sections
		// End of informational text
	}

}
