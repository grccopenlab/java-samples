public class MealData
{
	public static int FatCalories = 9;
	public static int CarbCalories = 4;
	public static int PrtnCalories = 4;
	
	public String mealName;
	public int gramsFat, gramsPrtn, gramsCarb;
	
	public MealData()
	{
		mealName = "";
		gramsFat = 0;
		gramsPrtn = 0;
		gramsCarb = 0;
	}
	
	public MealData(String meal)
	{
		this();
		mealName = meal;
	}
	
	public int mealCalories()
	{
		return (gramsFat * MealData.FatCalories) + (gramsPrtn * MealData.PrtnCalories) + (gramsCarb * MealData.CarbCalories);
	}
}
