package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * 
 * This class defines the properties of a Supplier for a business. Each supplier has
 * a contract term and supplies a given product. Suppliers can offer only one supply
 * in this definition of a supplier.
 * 
 * @author	Nathaniel Knight < nathaniel.d.knight @ gmail.com>
 * @version	2
 * @since	2015-11-03
 */
public class Supplier extends Person
{
	private String supplies;
	private int contractStart, contractEnd;

	/**
	 * Constructs a new Supplier from a name, name of supplies, and the start
	 * and end years of the supplier's contract.
	 * 
	 * @param	name	String name of this Supplier
	 * @param	supplies	String name of supplies provided in this contract
	 * @param	contractStart	Integer year in which the supplier contract begins
	 * @param	contractEnd	Integer year in which the supplier contract ends
	 */
	public Supplier(String name, String supplies, int contractStart, int contractEnd)
	{
		setName(name);
		setSupplies(supplies);
		setContract(contractStart, contractEnd);
	}
	
	/**
	 * Constructs a new Supplier as a copy of an existing Supplier.
	 * 
	 * @param	supplier	Supplier to copy into this Supplier
	 */
	public Supplier(Supplier supplier)
	{
		setName(supplier.getName());
		setSupplies(supplier.getSupplies());
		setContract(supplier.getContractStart(), supplier.getContractEnd());
	}
	
	/**
	 * 
	 * @return	String name of supplies provided by this supplier contract
	 */
	public String getSupplies()
	{ return supplies; }

	/**
	 * Sets the name of the supplies provided by this contract.
	 * 
	 * @param	supplies	String name to assign to supplies
	 */
	public void setSupplies(String supplies)
	{ this.supplies = supplies; }

	/**
	 * 
	 * @return Integer year in which the supplier contract begins
	 */
	public int getContractStart()
	{ return contractStart; }

	/**
	 * Sets the start year of the supplier contract.
	 * 
	 * @param	contractStart	Integer year to set to contractStart
	 */
	private void setContractStart(int contractStart)
	{ this.contractStart = contractStart; }

	/**
	 * 
	 * @return	Integer year in which the supplier contract ends
	 */
	public int getContractEnd()
	{ return contractEnd; }

	/**
	 * Sets the end year of the supplier contract.
	 * 
	 * @param	contractEnd	Integer year to set to contractEnd
	 */
	private void setContractEnd(int contractEnd)
	{ this.contractEnd = contractEnd; }
	
	/**
	 * Sets a contract term from integer years representing the start and
	 * end years for the contract.
	 * 
	 * @param	start	Integer year to set to contractStart
	 * @param	end	Integer year to set to contractEnd
	 */
	public void setContract(int start, int end)
	{
		setContractStart(start);
		setContractEnd(end);
	}

	/**
	 * A static builder method to create a new Suplier with validated input.
	 * 
	 * @param input	a Scanner to serve as an input source
	 * @return	a validated, well-formed Supplier object
	 */
	public static Supplier newSupplier(Scanner input)
	{
		String name;
		String supplies;
		int contractStart, contractEnd;
		boolean valid = false;
		
		do
		{
			name = Example08.prompt("Please enter the name of a supplier: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			supplies = Example08.prompt("What supply is this contract for? ", input);
			if(supplies.equals(""))
			{
				System.out.println("Must supply something.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		contractStart = Integer.valueOf(Example08.prompt("What year did this contract start? ", input));
		do
		{
			contractEnd = Integer.valueOf(Example08.prompt("What year will this contract end? ", input));
			if(contractEnd < contractStart)
			{ System.out.println("Contracts cannot end before they begin." ); }
			else
			{ valid = true; }
		} while (!valid);
		
		Supplier supplier = new Supplier(name, supplies, contractStart, contractEnd);
		return supplier;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " supplies " + getSupplies());
		sb.append(" until " + getContractEnd() + ".");
		return sb.toString();
	}
}
