package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This class defines the attributes of a Customer for our directory program.
 * This file should accompany Example08.java
 * 
 * @author Nathaniel Knight
 * @version 2
 * @since 2015-11-03
 * @modified 2015-11-03
 * 
 */
public class Customer extends Person
{
	private int purchases;
	private boolean loyaltyMember;

	/**
	 * Constructs a new Customer from a name, number of purchases, and a
	 * value for membership in the loyalty program.
	 * 
	 * @param	name	String name for this Customer
	 * @param	purchases	Integer number of purchases the customer has made
	 * @param	loyalty	Boolean whether or not customer has joined loyalty program
	 */
	public Customer(String name, int purchases, boolean loyalty)
	{
		setName(name);
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}

	/**
	 * Constructs a new Customer from a Person, number of purchases, and a
	 * value for membership in the loyalty program.
	 * 
	 * @param	person	Person to base this Customer on
	 * @param	purchases	Integer number of purchases the customer has made
	 * @param	loyalty	Boolean whether or not customer has joined loyalty program
	 */
	public Customer(Person person, int purchases, boolean loyalty)
	{
		setName(person.getName());
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}
	
	/**
	 * Constructs a new Customer as a copy of an existing Customer.
	 * 
	 * @param	customer	Customer to copy into this Customer
	 */
	public Customer(Customer customer)
	{
		setName(customer.getName());
		setPurchases(customer.getPurchases());
		setLoyaltyMember(customer.isLoyaltyMember());
	}

	/**
	 * 
	 * @return	the number of purchases made by this Customer
	 */
	public int getPurchases()
	{ return purchases; }

	/**
	 * 
	 * @param purchases	the number of purchases to be set for this Customer
	 */
	public void setPurchases(int purchases)
	{ this.purchases = purchases; }

	/**
	 * 
	 * @return	whether this Customer is a member of the loyalty program
	 */
	public boolean isLoyaltyMember()
	{ return loyaltyMember; }

	/**
	 * 
	 * @param loyaltyMember	the value to assign for membership in the loyalty program
	 */
	public void setLoyaltyMember(boolean loyaltyMember)
	{ this.loyaltyMember = loyaltyMember; }
	
	/**
	 * A static builder method to create a new Customer with validated input.
	 * 
	 * @param input	a Scanner to serve as an input source
	 * @return	a validated, well-formed Customer object
	 */
	public static Customer newCustomer(Scanner input)
	{
		String name;
		int purchases;
		boolean loyaltyMember = false;
		boolean valid = false;
		
		do
		{
			name = Example08.prompt("Please enter the name of a customer: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			purchases = Integer.valueOf(Example08.prompt("how many purchases has this customer made? ", input));
			if(purchases > 0)
			{
				valid = true;
			}
			else				
			{
				System.out.println("Invalid number of purchases.");
			}
		} while (!valid);
		
		if(purchases >= 5)
		{
			valid = false;
			
			do
			{
				loyaltyMember = Boolean.valueOf(Example08.prompt("Is this customer a member of the loyalty program (true or false)? ", input));
				if(loyaltyMember || !loyaltyMember)
				{ valid = true; }
			} while (!valid);
		}
		
		Customer customer = new Customer(name, purchases, loyaltyMember);
		return customer;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " is ");
		if(!isLoyaltyMember())
		{ sb.append("not "); }
		sb.append("a loyalty club member.");
		return sb.toString();
	}
}
