package edu.grcc.directory_sample;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is the eighth pass on a business directory project.
 * 
 * In this version, we modify the program to extract methods from
 * the original main() method. This allows us to easily adapt the
 * program to run in different modes, such as if we later want
 * both a console version and a version with a graphical user
 * interface. We are not currently implementing any new modes,
 * only a construction that allows us to add new modes.
 * 
 * Many of these methods are made by using Eclipse and its
 * refactoring tools, namely the "Extract Method" tool. The same
 * effect can be achieved by cutting and pasting lines of code,
 * but the automated tools in Eclipse are readily available.
 * 
 * @author Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version 6
 * @since 2015-09-30
 * @modified 2015-11-03
 *
 */
public class Example08 {
	public static final char QUIT = 'q';

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		menuApp(input);
	}	

	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
	
	/**
	 * Takes in a {@linkplain String} and a char, and compares the first character of
	 * the string to the char. Also takes a {@linkplain Boolean} to determine if this
	 * comparison is case sensitive or not. Note that the char must be a non-uppercase
	 * letter for case-insensitive comparisons.
	 * 
	 * @param string		a {@linkplain String} to check
	 * @param c				a char to check for
	 * @param caseSensitive	a bool to define if the check is case sensitive
	 * @return
	 */
	public static boolean compareFirstCharTo(String string, char c, boolean caseSensitive)
	{
		if(caseSensitive) { return string.charAt(0) == c; }
		return string.toLowerCase().charAt(0) == c;
	}
	
	/**
	 * Runs the program as a console-based, menu-driven application.
	 * 
	 * @param	input	A Scanner to be used for getting input throughout the program
	 */
	private static void menuApp(Scanner input)
	{
		String option = "";
		ArrayList<Person> people = new ArrayList<Person>(10);
		
		while(true)
		{
			option = mainMenu(input);

			if(Integer.valueOf(option) == 1)	// Menu Option 1: Add Person
			{
				people.add(addPerson(input));
			}
			
			if(Integer.valueOf(option) == 2)	// Menu Option 2: Delete Last Person
			{
				System.out.println("Removing " + people.get(people.size()-1).getName() + ".");
				people.remove(people.size()-1);
			}
			
			if(Integer.valueOf(option) == 3)	// Menu Option 3: Review All People
			{
				listPeople(people);
			}

			option = "";
		}
	}

	/**
	 * Runs the main menu.
	 */
	private static String mainMenu(Scanner input)
	{
		String option = " ";
		
		option = showMainMenu(input);
		if(compareFirstCharTo(option, 'q', false)) { quitApp(input); }
		if((Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3)) { return option; }
		return option;
	}

	/**
	 * This method will clean up after and close the program.
	 * 
	 * @param	input	A Scanner to close before the program ends.
	 */
	private static void quitApp(Scanner input)
	{
		input.close();
		System.exit(0);
	}

	/**
	 * Displays a list of all the Person objects in the ArrayList people.
	 * 
	 * @param	people	An ArrayList of Person objects to display.
	 */
	private static void listPeople(ArrayList<Person> people)
	{
		System.out.println("Listing all people:");
		for (Person person : people)
		{
			System.out.println(person.toString());
		}
		System.out.println("");
	}

	/**
	 * Commands to process input for a new Person in the directory.
	 * Will return a person after it has created one.
	 * 
	 * @param	input	A Scanner used to gather input throughout the program.
	 * 
	 * @return	A fully-formed Person to be used elsewhere
	 */
	private static Person addPerson(Scanner input)
	{
		String option;
		boolean valid = false;
		Person person;

		do
		{
			option = personMenu(input);
			if(Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3) { valid = true; }
		} while (!valid);
		
		if(Integer.valueOf(option) == 1) { person = Customer.newCustomer(input); }
		else if(Integer.valueOf(option) == 2) { person = Employee.newEmployee(input); }
		else if(Integer.valueOf(option) == 3) { person = Supplier.newSupplier(input); }
		else { person = null; }
		return person;
	}
	
	/**
	 * Shows the main menu for the program.
	 */
	private static String showMainMenu(Scanner input)
	{
		System.out.println("Please select an option below:");
		System.out.println("1. Add a person");
		System.out.println("2. Remove the last person");
		System.out.println("3. Review all people");
		System.out.println("Q. Quit this program");
		return input.nextLine();
	}
	
	/**
	 * Shows the relationship menu for new people.
	 */
	private static String personMenu(Scanner input)
	{
		System.out.println("Please select which of the following best describes this person:");
		System.out.println("1. Customer");
		System.out.println("2. Employee");
		System.out.println("3. Supplier");
		return input.next();
	}
}
