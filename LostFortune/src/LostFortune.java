import java.util.Scanner;

public class LostFortune
{
	public static void main(String[] args)
	{
		int GOLD_COINS = 900;
		Scanner input = new Scanner(System.in);

		int groupSize, groupDied, groupSurvived;
		String leaderName;
		
		System.out.println("Welcome to Lost Fortune. We'll need a few details before your adventure begins.");
		
		System.out.print("Please select a whole number larger than one: ");
		groupSize = Integer.valueOf(input.nextInt());
		
		System.out.print("Please select another whole number, zero or larger: ");
		groupDied = Integer.valueOf(input.nextInt());
		if(groupDied > groupSize) { groupDied = groupSize; }
		groupSurvived = groupSize - groupDied;
		
		System.out.print("Please enter a name for a brave hero: ");
		leaderName = input.next();
		
		// Begin Story Here
		System.out.println("Long ago, when might ruled over wisdom, a band of heroes set out for adventure.");
		System.out.println("These " + groupSize + " bold heroes set forth to the mountains, seeking the long lost gold of Dwarfhome.");
		System.out.println("The band was led by the most famous of heroes, " + leaderName + ", who led them far and wide.");
		System.out.println("Until the night the storm struck hard, killing " + groupDied/8 + " of the brave.");
		System.out.println("Disaster remained near from that night on. Goblins and ogres, wolves and bears, they carved the forces down.");
		System.out.println("Until of " + groupSize + " brave souls, only " + groupSurvived + " remained.");
		System.out.println("Yet they found it, after all their toils: the lost Dwarfhome and all its spoils.");
		System.out.println("When tallied up, " + GOLD_COINS + " coins were found, the living shared them all around.");
		System.out.println("Each survivor carried his share, " + (GOLD_COINS / groupSurvived) + " coins in all.");
		System.out.println(leaderName + " kept the smallest share, to pay for the rites of the fallen.");
		System.out.println("Fame, not gold, was " + leaderName + "'s reward, as the tale of Lost Treasure drew to a close.");
		
		input.close();
	}
}
