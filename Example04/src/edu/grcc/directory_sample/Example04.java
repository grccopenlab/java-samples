package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This is the fourth pass on a business directory project.
 * 
 * This pass adds simple input validation to the Person class loops and selections,
 * plus a validation method in Person which reviews if a Person has valid values
 * for name and relationship. With the exception of removed comments for old work,
 * Example04.java is identical to Example03.java
 *  
 * @author Nathaniel Knight
 * @version 4
 * @since 2015-09-30
 *
 */
public class Example04
{
	/**
	 * Builds two people, then outputs their names and relationships on separate
	 * lines.
	 * 
	 * @param	args	unused at present
	 */
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		Person person1;
		Person person2;
		
		person1 = Person.newPerson(input);
		person2 = Person.newPerson(input);
		System.out.println("");
		
		System.out.println(person1.getName() + " is one of your " + person1.getRelationship() + "s.");
		System.out.println(person2.getName() + " is one of your " + person2.getRelationship() + "s.");
		
		input.close();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
}
