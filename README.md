# README #

Java-Samples is a collection of sample project files for the Java course. These projects roughly accompany each week of CIS-117, along with a loose program requirements document. Most of these requirement documents also suggest ways a student might add to the program between lessons, with only the knowledge covered in class to the week in question.

The Example folders contain the tutorial projects.