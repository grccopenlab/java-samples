package edu.grcc.directory_sample;

import java.io.IOException;
import java.util.Scanner;

/**
 * This class defines the properties of an Employee working for a company. Employees
 * are hired to work a certain number of hours per work, and paid an hourly rate for
 * the hours they are scheduled to work. They may not be paid less than the proper
 * minimum wage, and are paid overtime
 * 
 * @author	Nathaniel Knight <nathanielknight @ gmail.com>
 * @version	07
 * @since	06
 */
public class Employee extends Person {
	private static final int FULLTIME_HOURS = 40;
	public static final double MINIMUM_WAGE = 8.05;
	public static final double MINIMUM_OT = 1.5;
	private int hours;
	private double payRate;
	private double overtimeRate;
	private boolean fullTime;

	public Employee(String name, int hours, double payRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(MINIMUM_OT);
	}
	
	public Employee(String name, int hours, double payRate, double overtimeRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(overtimeRate);
	}
	
	public Employee(Employee employee)
	{
		setName(employee.getName());
		setHours(employee.getHours());
		setPayRate(employee.getPayRate());
		setOvertimeRate(employee.getOvertimeRate());
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
		setFullTime();
	}

	public double getPayRate()
	{
		return payRate;
	}

	public void setPayRate(double payRate)
	{
		this.payRate = payRate;
	}
	
	private void setFullTime()
	{
		if(hours >= FULLTIME_HOURS) { fullTime = true; }
		else { fullTime = false; }
	}

	public boolean isFullTime()
	{
		return fullTime;
	}
	
	/**
	 * This method collects and validates the information for a new Employee; it will only
	 * allow technically valid values to be passed to the actual constructor call at the
	 * end; any invalid type entry will repeat the latest prompt, while invalid values
	 * will discard the inputs gathered so far and jump back up to the calling method to
	 * handle errors.
	 * 
	 * @param	input	->	The Input device used to get user input
	 * @return	Outputs an Employee object to be used in the Employee(Employee) constructor
	 * @throws IOException, NumberFormatException 
	 */
	public static Employee newEmployee(Scanner input) throws IOException, NumberFormatException
	{
		String name = "";
		int hours = 0;
		double payRate = 0;
		double overtimeRate = 0;
		
		while(name.trim().equals(""))
		{
			name = Example10.prompt("Please enter the name of an employee: ", input);
			if(name.trim().equals("")) { throw new IOException("name"); }
		}
		
		while(hours<=0)
		{
			hours = Integer.valueOf(Example10.prompt("How many hours does this employee work per week? ", input));
			if(hours <= 0 || hours > (24*7)) { throw new IOException("hours"); }
		}
		
		while(payRate < MINIMUM_WAGE)
		{
			try
			{
				payRate = Double.valueOf(Example10.prompt("how much is this employee paid per hour? ", input));
			}
			catch(NumberFormatException n) { System.out.println("Must enter a number."); }
			
			//Employees cannot be paid below the minimum wage
			if(payRate < MINIMUM_WAGE) { throw new NumberFormatException("payRate"); }
		}
		
		while(overtimeRate < MINIMUM_OT)
		{
			try
			{
				overtimeRate = Double.valueOf(Example10.prompt("What percentage of normal pay does this employee get for overtime? ", input));
			}
			catch(NumberFormatException n) { System.out.println("Must enter a number."); }
			if(overtimeRate < MINIMUM_OT) { throw new NumberFormatException("otPay"); }
		}
		
		Employee employee = new Employee(name, hours, payRate, overtimeRate);
		return employee;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + " is ");
		if(!isFullTime())
		{ sb.append("not "); }
		sb.append("a full-time employee.");
		return sb.toString();
	}

	public double getOvertimeRate() {
		return overtimeRate;
	}

	public void setOvertimeRate(double overtimeRate) {
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * This method calculates how much an employee is paid per paycheck
	 * 
	 * @return	A double value representing the value of the paycheck
	 */
	public double getPaycheck()
	{
		double pay = 0d;

		if(getHours()>40)	// This calculates the pay for overtime employees
		{
			pay += 40 * getPayRate();
			pay += (getHours() - 40) * (getPayRate() * getOvertimeRate());
		}
		else
		{ pay += getHours() * getPayRate(); }
		return pay;
	}
	
	@Override
	public String saveData() {
		// TODO Auto-generated method stub
		String output = "Employee\t";
		output += getName() + "\t";
		output += getHours() + "\t";
		output += getPayRate() + "\t";
		output += getOvertimeRate() + "\n";
		return output;
	}
}
