package edu.grcc.directory_sample;

import java.io.IOException;
import java.util.Scanner;

public class Customer extends Person {
	
	private int purchases;
	private boolean loyaltyMember;

	public Customer(String name, int purchases, boolean loyalty) {
		setName(name);
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}

	public Customer(Person person, int purchases, boolean loyalty) {
		setName(person.getName());
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}
	
	public Customer(Customer customer)
	{
		setName(customer.getName());
		setPurchases(customer.getPurchases());
		setLoyaltyMember(customer.isLoyaltyMember());
	}

	public int getPurchases() {
		return purchases;
	}

	public void setPurchases(int purchases) {
		this.purchases = purchases;
	}

	public boolean isLoyaltyMember() {
		return loyaltyMember;
	}

	public void setLoyaltyMember(boolean loyaltyMember) {
		this.loyaltyMember = loyaltyMember;
	}
	
	public static Customer newCustomer(Scanner input) throws IOException
	{
		String name = "";
		int purchases = 0;
		boolean loyaltyMember = false;
		
		while(name.equals(""))
		{
			name = Example10.prompt("Please enter the name of a customer: ", input);
			if(name.trim().equals("")) { throw new IOException("name"); }
		}
		
		while(purchases<=0)
		{
			try
			{
				purchases = Integer.valueOf(Example10.prompt("How many purchases has this customer made? ", input));
			}
			catch(NumberFormatException e)
			{ System.out.println("Must enter a number."); }
			if(purchases <= 0)
			{ throw new IOException("purchases"); }
		}
		
		if(purchases >= 5)
		{
			loyaltyMember = Boolean.valueOf(Example10.prompt("Is " + name + " a member of the loyalty program (true or false)? ", input));
		}
		
		Customer customer = new Customer(name, purchases, loyaltyMember);
		return customer;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " is ");
		if(!isLoyaltyMember()) { sb.append("not "); }
		sb.append("a loyalty club member.");
		return sb.toString();
	}
	
	@Override
	public String saveData() {
		// TODO Auto-generated method stub
		String output = "Supplier\t";
		output += getName() + "\t";
		output += getPurchases() + "\t";
		output += isLoyaltyMember() + "\n";
		return output;
	}
}
