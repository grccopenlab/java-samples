package edu.grcc.directory_sample;

import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * This class defines the properties of a Supplier for a business. Each supplier has
 * a contract term and supplies a given product. Suppliers can offer only one supply
 * in this definition of a supplier.
 * 
 * @author	Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version	07
 * @since	06
 */
public class Supplier extends Person {
	
	private String supplies;
	private int contractStart, contractEnd;

	public Supplier(String name, String supplies, int contractStart, int contractEnd)
	{
		setName(name);
		setSupplies(supplies);
		setContract(contractStart, contractEnd);
	}
	
	public Supplier(Supplier supplier)
	{
		setName(supplier.getName());
		setSupplies(supplier.getSupplies());
		setContract(supplier.getContractStart(), supplier.getContractEnd());
	}
	
	public String getSupplies() {
		return supplies;
	}

	public void setSupplies(String supplies) {
		this.supplies = supplies;
	}

	public int getContractStart() {
		return contractStart;
	}

	private void setContractStart(int contractStart) {
		this.contractStart = contractStart;
	}

	public int getContractEnd() {
		return contractEnd;
	}

	private void setContractEnd(int contractEnd) {
		this.contractEnd = contractEnd;
	}
	
	public void setContract(int start, int end)
	{
		setContractStart(start);
		setContractEnd(end);
	}

	public static Supplier newSupplier(Scanner input) throws IOException, NumberFormatException
	{
		String name = "";
		String supplies = "";
		int contractStart = 1900;
		int contractEnd = 1900;
		while(name.trim().equals(""))
		{
			name = Example10.prompt("Please enter the name of a supplier: ", input);
			if(name.trim().equals("")) { throw new IOException("name"); }
		}
		
		while(supplies.trim().equals(""))
		{
			supplies = Example10.prompt("What supply is this contract for? ", input);
			if(supplies.trim().equals("")) { throw new IOException("name"); }
		}
		
		while(contractStart <= 1900)
		{
			try
			{
				contractStart = Integer.valueOf(Example10.prompt("What year did this contract start? ", input));
			}
			catch(NumberFormatException n) { System.out.println("Must enter a number"); }
			if(contractStart <=1900) { throw new IOException("yearStart"); }
		}
		
		while(contractEnd < contractStart)
		{
			try
			{
				contractEnd = Integer.valueOf(Example10.prompt("What year will this contract end? ", input));
			}
			catch(NumberFormatException n) { System.out.println("Must enter a number"); }
			if(contractEnd < contractStart) { throw new IOException("yearEnd"); }
		}
		
		Supplier supplier = new Supplier(name, supplies, contractStart, contractEnd);
		return supplier;
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " supplies " + getSupplies());
		sb.append(" until " + getContractEnd() + ".");
		return sb.toString();
	}

	@Override
	public String saveData() {
		// TODO Auto-generated method stub
		String output = "Supplier\t";
		output += getName() + "\t";
		output += getSupplies() + "\t";
		output += getContractStart() + "\t";
		output += getContractEnd() + "\n";
		return output;
	}
}
