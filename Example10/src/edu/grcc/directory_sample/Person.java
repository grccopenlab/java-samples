package edu.grcc.directory_sample;

/**
 * 
 * This class defines the abstract properties of a person, to be implemented in individual
 * classes by type of person.
 * 
 * @author	Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version	07
 * @since	02
 */
public abstract class Person {
	protected String name;
	
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}

	public abstract String toString();
	
	public abstract String saveData();
}
