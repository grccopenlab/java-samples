package edu.grcc.directory_sample;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * This class provides the entry point and program logic for a business contact list.
 *  
 * @author	Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version	10
 * @since	02
 */
public class Example10 {
	public static final char QUIT = 'q';

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		menuApp(input);
	}
	
	private static void menuApp(Scanner input)
	{
		String option = "";
		ArrayList<Person> people = new ArrayList<Person>(10);
		
		
		while(true)
		{
			while(option.equals(""))
			{
				try { option = mainMenu(input); }
				catch(Exception e) { System.out.println("Error: Must enter 1-3 or 'q'."); }
			}

			if(Integer.valueOf(option) == 1)	// Menu Option 1: Add Person
			{
				int goalsize = people.size()+1;

				while(people.size() != goalsize)
				{
					try { people.add(addPerson(input)); }
					catch(Exception e) {  }
				}
			}
			
			if(Integer.valueOf(option) == 2)	// Menu Option 2: Delete Last Person
			{
				
					try
					{
						System.out.println("Removing " + people.get(people.size()-1).getName() + ".");
						people.remove(people.size()-1);
					}
					catch(IndexOutOfBoundsException e) { System.out.println("No record to remove."); }
			}
			
			if(Integer.valueOf(option) == 3)	// Menu Option 3: Review All People
			{ listPeople(people); }

			option = "";
		}
	}

	private static String mainMenu(Scanner input) throws IOException {
		String option = " ";
		
		option = showMainMenu(input);
		
		// This provides us with a termination -- if the user enters 'q' at
		// the start of their response, we quit immediately
		if(compareFirstCharTo(option, 'q', false)) { quitApp(input); }
		
		// the termination means the below only happens for anything except 'q'
		if((Integer.valueOf(option) >= 1 && Integer.valueOf(option) <= 3)) { return option; }
		else { throw new IOException(); }
	}

	private static void quitApp(Scanner input) {
		input.close();
		System.out.println("Program closed");
		System.exit(0);
	}

	private static void listPeople(ArrayList<Person> people) {
		System.out.println("Listing all people:");
		for (Person person : people)
		{
			try { System.out.println(person.toString()); }
			catch(NullPointerException n) {  }
		}
		System.out.println("");
	}

	private static Person addPerson(Scanner input) throws IOException {
		String option = "";
		Person person = null;

		while (option.equals(""))
		{
			option = personMenu(input);
			if(Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3) { }
			else { throw new IOException(); }
		}
		
		while(person == null)
		{
			try
			{
				if(Integer.valueOf(option) == 1) { person =  Customer.newCustomer(input); }
				else if(Integer.valueOf(option) == 2) { person = Employee.newEmployee(input); }
				else if(Integer.valueOf(option) == 3) { person =  Supplier.newSupplier(input); }
				else { person = null; }
			}
			catch(Exception e)
			{ 
				if(e.getMessage().equals("name"))
				{ System.out.println("Names of persons or supplies may not be blank."); }
				
				if(e.getMessage().equals("purchases"))
				{ System.out.println("Number of purchases must be 1 or greater."); }
				
				if(e.getMessage().equals("hours"))
				{ System.out.println("Employees must work a positive, non-zero number of hours, and no more than " + (24*7) + " hours."); }
				
				if(e.getMessage().equals("payRate"))
				{ System.out.println("No employee may be paid less than $" + Employee.MINIMUM_WAGE + " per hour."); }
				
				if(e.getMessage().equals("otPay"))
				{ System.out.println("All employees must receive at least time-and-a-half, or " + Employee.MINIMUM_OT + " times base pay for overtime."); }
				
				if(e.getMessage().equals("yearStart"))
				{ System.out.println("Contracts may not begin before 1901."); }
				
				if(e.getMessage().equals("yearEnd"))
				{ System.out.println("No contract may end before it begins."); }				
			}
			//break;
		}
		
		return person;
	}
	
	private static String showMainMenu(Scanner input)
	{
		System.out.println("Please select an option below:");
		System.out.println("1. Add a person");
		System.out.println("2. Remove the last person");
		System.out.println("3. Review all people");
		System.out.println("Q. Quit this program");
		return input.nextLine();
	}
	
	private static String personMenu(Scanner input)
	{
		System.out.println("Please select which of the following best describes this person:");
		System.out.println("1. Customer");
		System.out.println("2. Employee");
		System.out.println("3. Supplier");
		return input.nextLine();
	}
	
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
	
	public static boolean compareFirstCharTo(String string, char c, boolean caseSensitive)
	{
		if(caseSensitive) { return string.charAt(0) == c; }
		return string.toLowerCase().charAt(0) == c;
	}
}
