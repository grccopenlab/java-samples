package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * 
 * This class defines the properties of an Employee working for a company. Employees
 * are hired to work a certain number of hours per work, and paid an hourly rate for
 * the hours they are scheduled to work. They may not be paid less than the proper
 * minimum wage, and are paid overtime for all hours over 40.
 * 
 * @author	Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version	2
 * @since 2015-11-03
 * @modified 2016-01-14
 * 
 * Modified 2016 Jan 14: MINIMUM_WAGE updated to reflect Michigan Minimum Wage
 * 
 */
public class Employee extends Person
{
	private static final int FULLTIME_HOURS = 40;
	private static final double MINIMUM_WAGE = 8.50;
	private static final double MINIMUM_OT = 1.5;
	private int hours;
	private double payRate;
	private double overtimeRate;
	private boolean fullTime;

	/**
	 * Constructs a new Employee from a name, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	name	String name for this Customer
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(String name, int hours, double payRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(MINIMUM_OT);
	}
	
	/**
	 * Constructs new Employee from a Person, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	person	Person to base this Employee on
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(String name, int hours, double payRate, double overtimeRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(overtimeRate);
	}
	
	/**
	 * Constructs a new Employee as a copy of an existing Employee.
	 * 
	 * @param	employee	Employee to copy into this Employee
	 */
	public Employee(Employee employee)
	{
		setName(employee.getName());
		setHours(employee.getHours());
		setPayRate(employee.getPayRate());
		setOvertimeRate(employee.getOvertimeRate());
	}

	/**
	 * 
	 * @return	Integer hours worked per week
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * Sets the number of hours worked per week.
	 * 
	 * @param	hours	Integer to set to hours
	 */
	public void setHours(int hours) {
		this.hours = hours;
		setFullTime();
	}

	/**
	 * 
	 * @return	Double dollar value of pay per hour worked
	 */
	public double getPayRate()
	{
		return payRate;
	}

	/**
	 * Sets the wages or rate of pay per hour.
	 * 
	 * @param	payRate	Double dollar value to set to payRate
	 */
	public void setPayRate(double payRate)
	{
		this.payRate = payRate;
	}
	
	/**
	 * Compares the current value of hours to the minimum value required for
	 * full-time status, and sets the fullTime value by the result.
	 */
	private void setFullTime()
	{
		if(hours >= FULLTIME_HOURS) { fullTime = true; }
		else { fullTime = false; }
	}

	/**
	 * 
	 * @return	whether this Employee qualifies as full-time
	 */
	public boolean isFullTime()
	{
		return fullTime;
	}
	
	/**
	 * 
	 * This method collects and validates the information for a new Employee; it will only
	 * allow technically valid values to be passed to the actual constructor call at the
	 * end.
	 * 
	 * @param	input	a Scanner used to get user input
	 * @return	Outputs an Employee object to be used in the Employee(Employee) constructor
	 */
	public static Employee newEmployee(Scanner input)
	{
		String name;
		int hours;
		double payRate, overtimeRate;
		boolean valid = false;
		
		do
		{
			name = Example07.prompt("Please enter the name of an employee: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			hours = Integer.valueOf(Example07.prompt("How many hours does this employee work per week? ", input));
			
			// Employees cannot work less than 0 hours
			if(hours >= 0)
			{
				valid = true;
			}
			else
			{
				System.out.println("Cannot work negative hours.");
			}
		} while (!valid);
		
		valid = false;
		
		do
		{
			payRate = Double.valueOf(Example07.prompt("how much is this employee paid per hour? ", input));
			
			//Employees cannot be paid below the minimum wage
			if(payRate >= MINIMUM_WAGE)
			{
				valid = true;
			}
			else
			{
				System.out.println("Employees must be paid minimum wage.");
			}
		} while (!valid);
		
valid = false;
		
		do
		{
			overtimeRate = Double.valueOf(Example07.prompt("What percentage of normal pay does this employee get for overtime? ", input));
			if(overtimeRate >= MINIMUM_OT)
			{
				valid = true;
			}
			else
			{
				System.out.println("Employees must receive 150% (1.5) base pay for overtime.");
			}
		} while (!valid);
		
		Employee employee = new Employee(name, hours, payRate, overtimeRate);
		return employee;
	}

	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + " is ");
		if(!isFullTime())
		{ sb.append("not "); }
		sb.append("a full-time employee.");
		return sb.toString();
	}

	/**
	 * 
	 * @return	double value of overtime pay ratio
	 */
	public double getOvertimeRate() {
		return overtimeRate;
	}

	/**
	 * Sets the value of the overtime ratio. Ensures that all overtime is paid with
	 * at least the minimum ratio.
	 * 
	 * @param overtimeRate
	 */
	public void setOvertimeRate(double overtimeRate) {
		if(overtimeRate < MINIMUM_OT)
		{
			this.overtimeRate = MINIMUM_OT;
			return;
		}
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * Outputs the double amount that will be paid to the employee in their paycheck.
	 * 
	 * @return	A double value representing the monetary value of the paycheck
	 */
	public double getPaycheck()
	{
		double pay = 0d;

		if(getHours()>40)	// This calculates the pay for overtime employees
		{
			pay += 40 * getPayRate();
			pay += (getHours() - 40) * (getPayRate() * getOvertimeRate());
		}
		else
		{
			pay += getHours() * getPayRate();
		}
		return pay;
	}
}
