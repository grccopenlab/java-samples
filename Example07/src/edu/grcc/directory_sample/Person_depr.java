package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This is the remnant of the original Person.java file. It is preserved
 * in this project for the sake of illustrating the changes made from
 * this original codebase to the abstract version of the file represented
 * in the new Person.java file.
 * 
 * @author Nathaniel Knight < nathaniel.d.knight @ gmail.com >
 * @version 6
 * @since 2015-09-30
 * @modified 2015-11-03
 * @deprecated
 * 
 */
public class Person_depr {
	private String name;
	private String relationship;
	
	/**
	 * Constructs a new Person using a string for name and another string
	 * for relationship.
	 * 
	 * @param	name	the name of the person
	 * @param	relationship	the relationship of the person to the user
	 */
	Person_depr(String name, String relationship)
	{
		setName(name);
		setRelationship(relationship);
	}
	
	/**
	 * Constructs a Person from an existing Person, copying the name
	 * and relationship attributes from the instance passed in.
	 * 
	 * @param	basePerson	the Person being used as a base
	 */
	Person_depr(Person_depr person)
	{
		this.name = person.getName();
		this.relationship = person.getRelationship();
	}

	/**
	 * 
	 * @return	the name of this object
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name value of this object
	 * 
	 * @param	name	the value to assign to name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return		the relationship value of this object
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * Set the relationship value of this object
	 * 
	 * @param	relationship	the value to assign to relationship
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	/**
	 * A builder method to collect the information needed to create a new
	 * instance of Person. Includes internal error checking of the user's
	 * entered data.
	 * 
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a new Person to be used elsewhere in the program
	 */
	public static Person_depr newPerson(Scanner input)
	{
		String name, relationship;
		boolean valid = false;
		
		do
		{
			name = Example07.prompt("Please enter the name of a person: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			relationship = Example07.prompt("Is this person a customer, employee, or supplier? ", input);
			if(relationship.equalsIgnoreCase("customer") || relationship.equalsIgnoreCase("employee") || relationship.equalsIgnoreCase("supplier"))
			{
				valid = true;
			}
			else				
			{
				System.out.println("Invalid name.");
			}
		} while (!valid);
		
		Person_depr person = new Person_depr(name, relationship);
		return person;
	}

	/**
	 * Validates a Person created through any method, particularly those
	 * created without the {@linkplain newPerson} builder method
	 * 
	 * @return		a {@linkplain Boolean} representing if the name or relationship
	 * values of this object are valid
	 */
	public boolean isValidPerson()
	{
		boolean valid = true;

		if(name == null || name.equals(""))
		{
			valid = false;
			System.out.println("Invalid name.");
		}
		if(!relationship.toLowerCase().equals("customer") &&
				!relationship.toLowerCase().equals("employee") &&
				!relationship.toLowerCase().equals("supplier"))
		{
			valid = false;
			System.out.println("Invalid relationship.");
		}
		
		return valid;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getName());
		if(getRelationship().toLowerCase().equals("employee"))
		{ sb.append(" is an "); }
		else
		{ sb.append(" is a "); }
		sb.append(getRelationship() + ".");
		return sb.toString();
	}
}
