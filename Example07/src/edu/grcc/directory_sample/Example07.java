package edu.grcc.directory_sample;
import java.util.ArrayList;
import java.util.Scanner;

public class Example07 {
	public static final char QUIT = 'q';

	/**
	 * This is the seventh pass on a business directory project.
	 * 
	 * In this version, we modify the program to use an abstract base
	 * class for directory entries, rather than a full class.
	 *  
	 * @author Nathaniel Knight <nathaniel.d.knight @ gmail.com>
	 * @version 6
	 * @since 2015-09-30
	 * @modified 2015-11-03
	 *
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String option = "";
		ArrayList<Person> people = new ArrayList<Person>(10);
		boolean valid = false;
		
		do	// start main program loop
		{
			do // start menu loop
			{
				valid = false;
				showMenu();
				option = input.nextLine();
				if(compareFirstTo(option, 'q', false))
				{ System.exit(0);; }
				if(!valid && (Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3))
				{ valid = true; }
			} while(!valid);	// end menu loop
			
			if(Integer.valueOf(option) == 1)	// Menu Option 1: Add Person
			{
				do
				{
					personMenu();
					option = input.nextLine();
					if(Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3)
					{ valid = true; }
				} while (!valid);
				
				if(Integer.valueOf(option) == 1) { people.add(Customer.newCustomer(input)); }
				if(Integer.valueOf(option) == 2) { people.add(Employee.newEmployee(input)); }
				if(Integer.valueOf(option) == 3) { people.add(Supplier.newSupplier(input)); }
				
				System.out.println(people.size());
				//System.out.println("Added " + people.get(people.size()-1).getName() + ".");
				option = "1"; // this resets our option choice before the if block ends
			}
			
			if(Integer.valueOf(option) == 2)	// Menu Option 2: Delete Last Person
			{
				System.out.println("Removing " + people.get(people.size()-1).getName() + ".");
				people.remove(people.size()-1);
			}
			
			if(Integer.valueOf(option) == 3)	// Menu Option 3: Review All People
			{
				System.out.println("Listing all people:");
				for(int i=0; i<people.size(); i++)
				{
					System.out.println(people.get(i).toString());
				}
				System.out.println("");
			}
		} while(option.toLowerCase().charAt(0) != QUIT);	// end main program loop
				
		input.close();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
	
	/**
	 * Takes in a {@linkplain String} and a char, and compares the first character of
	 * the string to the char. Also takes a {@linkplain Boolean} to determine if this
	 * comparison is case sensitive or not. Note that the char must be a non-uppercase
	 * letter for case-insensitive comparisons.
	 * 
	 * @param string		a {@linkplain String} to check
	 * @param c				a char to check for
	 * @param caseSensitive	a bool to define if the check is case sensitive
	 * @return
	 */
	public static boolean compareFirstTo(String string, char c, boolean caseSensitive)
	{
		if(caseSensitive) { return string.charAt(0) == c; }
		return string.toLowerCase().charAt(0) == c;
	}
	
	/**
	 * Shows the main menu for the program.
	 */
	public static void showMenu()
	{
		System.out.println("Please select an option below:");
		System.out.println("1. Add a person");
		System.out.println("2. Remove the last person");
		System.out.println("3. Review all people");
		System.out.println("Q. Quit this program");
	}
	
	/**
	 * Shows the relationship menu for new people.
	 */
	public static void personMenu()
	{
		System.out.println("Please select which of the following best describes this person:");
		System.out.println("1. Customer");
		System.out.println("2. Employee");
		System.out.println("3. Supplier");
	}
}
