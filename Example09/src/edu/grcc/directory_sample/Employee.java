package edu.grcc.directory_sample;
import java.io.IOException;
import java.util.Scanner;

/**
 * This class defines the properties of an Employee working for a company. Employees
 * are hired to work a certain number of hours per work, and paid an hourly rate for
 * the hours they are scheduled to work. They may not be paid less than the proper
 * minimum wage, and are paid overtime for all hours over 40.
 * 
 * As of this pass, the program includes exception handling code to resolve
 * errors, and allow the program to run through certain invalid inputs.
 * 
 * This file should accompany Example09.java
 * 
 * @author	Nathaniel Knight <nathanielknight @ gmail.com>
 * @version	3
 * @since	2015-11-03
 * @modified 2016-01-28
 * 
 * Modified 2016 Jan 28: MINIMUM_WAGE updated to reflect Michigan Minimum Wage
 * 
 */
public class Employee extends Person {
	private static final int FULLTIME_HOURS = 40;
	public static final double MINIMUM_WAGE = 8.50;
	public static final double MINIMUM_OT = 1.5;
	private int hours;
	private double payRate;
	private double overtimeRate;
	private boolean fullTime;

	/**
	 * Constructs a new Employee from a name, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	name	String name for this Customer
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(String name, int hours, double payRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(MINIMUM_OT);
	}
	
	/**
	 * Constructs new Employee from a Person, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	person	Person to base this Employee on
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(String name, int hours, double payRate, double overtimeRate)
	{
		setName(name);
		setHours(hours);
		setPayRate(payRate);
		setOvertimeRate(overtimeRate);
	}
	
	/**
	 * Constructs a new Employee as a copy of an existing Employee.
	 * 
	 * @param	employee	Employee to copy into this Employee
	 */
	public Employee(Employee employee)
	{
		setName(employee.getName());
		setHours(employee.getHours());
		setPayRate(employee.getPayRate());
		setOvertimeRate(employee.getOvertimeRate());
	}

	/**
	 * 
	 * @return	Integer hours worked per week
	 */
	public int getHours()
	{
		return hours;
	}

	/**
	 * Sets the number of hours worked per week.
	 * 
	 * @param	hours	Integer to set to hours
	 */
	public void setHours(int hours) {
		this.hours = hours;
		setFullTime();
	}

	/**
	 * 
	 * @return	Double dollar value of pay per hour worked
	 */
	public double getPayRate()
	{
		return payRate;
	}

	/**
	 * Sets the wages or rate of pay per hour.
	 * 
	 * @param	payRate	Double dollar value to set to payRate
	 */
	public void setPayRate(double payRate)
	{
		this.payRate = payRate;
	}
	
	/**
	 * Compares the current value of hours to the minimum value required for
	 * full-time status, and sets the fullTime value by the result.
	 */
	private void setFullTime()
	{
		if(hours >= FULLTIME_HOURS) { fullTime = true; }
		else { fullTime = false; }
	}

	/**
	 * 
	 * @return	whether this Employee qualifies as full-time
	 */
	public boolean isFullTime()
	{
		return fullTime;
	}
	
	/**
	 * This method collects and validates the information for a new Employee; it will only
	 * allow technically valid values to be passed to the actual constructor call at the
	 * end; any invalid type entry will repeat the latest prompt, while invalid values
	 * will discard the inputs gathered so far and jump back up to the calling method to
	 * handle errors.
	 * 
	 * @param	input	->	The Input device used to get user input
	 * @return	->	an Employee object to be used in the Employee(Employee) constructor
	 * @throws	IOException, NumberFormatException	->	Exceptions to be handled elsewhere in the program
	 */
	public static Employee newEmployee(Scanner input) throws IOException, NumberFormatException
	{
		String name = "";
		int hours = 0;
		double payRate = 0;
		double overtimeRate = 0;
		
		while(name.trim().equals(""))
		{
			name = Example09.prompt("Please enter the name of an employee: ", input);
			if(name.trim().equals(""))
			{
				throw new IOException("name"); // This exception sends a message "name" indicating a blank name; it will be handled elsewhere
			}
		}
		
		while(hours<=0)
		{
			hours = Integer.valueOf(Example09.prompt("How many hours does this employee work per week? ", input));
			if(hours <= 0 || hours > (24*7))
			{
				throw new IOException("hours"); // This exception sends a message "hours" indicating a not allowed number of hours; it will be handled elsewhere
			}
		}
		
		while(payRate < MINIMUM_WAGE)
		{
			try
			{
				payRate = Double.valueOf(Example09.prompt("how much is this employee paid per hour? ", input));
			}
			catch(NumberFormatException n) // this block handles invalid numbers, so we look for NumberFormatException
			{
				System.out.println("Must enter a number.");
			}
			
			//Employees cannot be paid below the minimum wage
			if(payRate < MINIMUM_WAGE)
			{
				throw new NumberFormatException("payRate"); // This exception sends a message "payRate" indicating too low of a pay rate; it will be handled elsewhere
			}
		}
		
		while(overtimeRate < MINIMUM_OT)
		{
			try
			{
				overtimeRate = Double.valueOf(Example09.prompt("What percentage of normal pay does this employee get for overtime? ", input));
			}
			catch(NumberFormatException n) // this block handles invalid numbers, so we look for NumberFormatException
			{
				System.out.println("Must enter a number.");
			}
			if(overtimeRate < MINIMUM_OT)
			{
				throw new NumberFormatException("otPay"); // This exception sends a message "otPay" indicating that overtime pay is too low; it will be handled elsewhere
			}
		}
		
		Employee employee = new Employee(name, hours, payRate, overtimeRate);
		return employee;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getName() + " is ");
		if(!isFullTime())
		{ sb.append("not "); }
		sb.append("a full-time employee.");
		return sb.toString();
	}

	/**
	 * 
	 * @return	double value of overtime pay ratio
	 */
	public double getOvertimeRate() {
		return overtimeRate;
	}

	/**
	 * Sets the value of the overtime ratio. Ensures that all overtime is paid with
	 * at least the minimum ratio.
	 * 
	 * @param overtimeRate
	 */
	public void setOvertimeRate(double overtimeRate) {
		this.overtimeRate = overtimeRate;
	}
	
	/**
	 * This method calculates how much an employee is paid per paycheck
	 * 
	 * @return	A double value representing the value of the paycheck
	 */
	public double getPaycheck()
	{
		double pay = 0d;

		if(getHours()>40)	// This calculates the pay for overtime employees
		{
			pay += 40 * getPayRate();
			pay += (getHours() - 40) * (getPayRate() * getOvertimeRate());
		}
		else
		{ pay += getHours() * getPayRate(); }
		return pay;
	}
}
