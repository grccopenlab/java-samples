package edu.grcc.directory_sample;
import java.io.IOException;
import java.util.Scanner;

/**
 * This class defines the attributes of a Customer for our directory program.
 * 
 * As of this pass, the program includes exception handling code to resolve
 * errors, and allow the program to run through certain invalid inputs.
 * 
 * This file should accompany Example09.java
 * 
 * @author Nathaniel Knight
 * @version 3
 * @since 2015-11-03
 * @modified 2015-11-03
 * 
 */
public class Customer extends Person
{
	private int purchases;
	private boolean loyaltyMember;

	/**
	 * Constructs a new Customer from a name, number of purchases, and a
	 * value for membership in the loyalty program.
	 * 
	 * @param	name	String name for this Customer
	 * @param	purchases	Integer number of purchases the customer has made
	 * @param	loyalty	Boolean whether or not customer has joined loyalty program
	 */
	public Customer(String name, int purchases, boolean loyalty)
	{
		setName(name);
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}

	/**
	 * Constructs a new Customer from a Person, number of purchases, and a
	 * value for membership in the loyalty program.
	 * 
	 * @param	person	Person to base this Customer on
	 * @param	purchases	Integer number of purchases the customer has made
	 * @param	loyalty	Boolean whether or not customer has joined loyalty program
	 */
	public Customer(Person person, int purchases, boolean loyalty)
	{
		setName(person.getName());
		setPurchases(purchases);
		setLoyaltyMember(loyalty);
	}
	
	/**
	 * Constructs a new Customer as a copy of an existing Customer.
	 * 
	 * @param	customer	Customer to copy into this Customer
	 */
	public Customer(Customer customer)
	{
		setName(customer.getName());
		setPurchases(customer.getPurchases());
		setLoyaltyMember(customer.isLoyaltyMember());
	}

	/**
	 * 
	 * @return	the number of purchases made by this Customer
	 */
	public int getPurchases() {
		return purchases;
	}

	/**
	 * 
	 * @param purchases	the number of purchases to be set for this Customer
	 */
	public void setPurchases(int purchases) {
		this.purchases = purchases;
	}

	/**
	 * 
	 * @return	whether this Customer is a member of the loyalty program
	 */
	public boolean isLoyaltyMember() {
		return loyaltyMember;
	}

	/**
	 * 
	 * @param loyaltyMember	the value to assign for membership in the loyalty program
	 */
	public void setLoyaltyMember(boolean loyaltyMember) {
		this.loyaltyMember = loyaltyMember;
	}
	
	/**
	 * A static builder method to create a new Customer with validated input.
	 * 
	 * @param	input	a Scanner to serve as an input source
	 * @return	a validated, well-formed Customer object
	 * @throws	an IOException with a message identifying where invalid input was provided
	 */
	public static Customer newCustomer(Scanner input) throws IOException
	{
		String name = "";
		int purchases = 0;
		boolean loyaltyMember = false;
		
		while(name.equals(""))
		{
			name = Example09.prompt("Please enter the name of a customer: ", input);
			if(name.trim().equals(""))
			{
				throw new IOException("name");	// This exception sends a message "name" indicating a blank name; it will be handled elsewhere
			}
		}
		
		while(purchases<=0)
		{
			try
			{
				purchases = Integer.valueOf(Example09.prompt("How many purchases has this customer made? ", input));
			}
			catch(NumberFormatException e) // this block handles invalid numbers, so we look for NumberFormatException
			{ System.out.println("Must enter a number."); }
			if(purchases <= 0)
			{
				throw new NumberFormatException("purchases"); // This exception sends a message "purchases" indicating an not allowed number; it will be handled elsewhere
			}
		}
		
		if(purchases >= 5)
		{
			loyaltyMember = Boolean.valueOf(Example09.prompt("Is " + name + " a member of the loyalty program (true or false)? ", input));
		}
		
		Customer customer = new Customer(name, purchases, loyaltyMember);
		return customer;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " is ");
		if(!isLoyaltyMember()) { sb.append("not "); }
		sb.append("a loyalty club member.");
		return sb.toString();
	}
}
