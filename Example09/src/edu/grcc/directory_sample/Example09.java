package edu.grcc.directory_sample;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is the eighth pass on a business directory project.
 * 
 * In this version, we add exception handling, so that our program
 * can continue running when invalid entries are provided, instead
 * of crashing and closing. We implement exceptions in every area
 * where users might enter data that is illegal in Java or that is
 * not allowed by our rules, so the program can return to a safe
 * point and continue running.
 * 
 * The rules we are using to decide where to set exceptions are all
 * listed in the tutorial text, which these files should accompany.
 * 
 * @author Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version 7
 * @since 2015-09-30
 * @modified 2015-11-03
 *
 */
public class Example09 {
	public static final char QUIT = 'q';

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		menuApp(input);
	}
	
	/**
	 * Runs the program as a console-based, menu-driven application.
	 * 
	 * @param	input	A Scanner to be used for getting input throughout the program
	 */
	private static void menuApp(Scanner input)
	{
		String option = "";
		ArrayList<Person> people = new ArrayList<Person>(10);
		
		while(true)
		{
			while(option.equals(""))
			{
				try
				{ option = mainMenu(input); }
				catch(Exception e) // This will catch any Exception, even though the mainMenu() method only throws an IOException
				{ System.out.println("Error: Must enter 1-3 or 'q'."); }
			}

			if(Integer.valueOf(option) == 1)	// Menu Option 1: Add Person
			{
				int goalsize = people.size()+1;

				while(people.size() != goalsize)
				{
					try { people.add(addPerson(input)); }
					catch(Exception e)
					{
						// This is a special exception handling case: We don't want to do anything with the exception, so we
						// leave the catch block empty of actual code.
					}
				}
			}
			
			if(Integer.valueOf(option) == 2)	// Menu Option 2: Delete Last Person
			{
				
					try
					{
						System.out.println("Removing " + people.get(people.size()-1).getName() + ".");
						people.remove(people.size()-1);
					}
					catch(IndexOutOfBoundsException e) // IndexOutOfBoundsException happens when you try to access part of an array or list that doesn't exist
					{ System.out.println("No record to remove."); }
			}
			
			if(Integer.valueOf(option) == 3)	// Menu Option 3: Review All People
			{ listPeople(people); }

			option = "";
		}
	}

	/**
	 * Runs the main menu.
	 */
	private static String mainMenu(Scanner input) throws IOException {
		String option = " ";
		
		option = showMainMenu(input);
		
		// This provides us with a termination -- if the user enters 'q' at
		// the start of their response, we quit immediately
		if(compareFirstCharTo(option, 'q', false)) { quitApp(input); }
		
		// the termination means the below only happens for anything except 'q'
		if((Integer.valueOf(option) >= 1 && Integer.valueOf(option) <= 3)) { return option; }
		else { throw new IOException(); }
	}

	/**
	 * This method will clean up after and close the program.
	 * 
	 * @param	input	A Scanner to close before the program ends.
	 */
	private static void quitApp(Scanner input) {
		input.close();
		System.out.println("Program closed");
		System.exit(0);
	}

	/**
	 * Displays a list of all the Person objects in the ArrayList people.
	 * 
	 * @param	people	An ArrayList of Person objects to display.
	 */
	private static void listPeople(ArrayList<Person> people) {
		System.out.println("Listing all people:");
		for (Person person : people)
		{
			try { System.out.println(person.toString()); }
			catch(NullPointerException n) {  }
		}
		System.out.println("");
	}

	/**
	 * Commands to process input for a new Person in the directory.
	 * Will return a person after it has created one.
	 * 
	 * @param	input	A Scanner used to gather input throughout the program.
	 * 
	 * @return	A fully-formed Person to be used elsewhere
	 */
	private static Person addPerson(Scanner input) throws IOException {
		String option = "";
		Person person = null;

		while (option.equals(""))
		{
			option = personMenu(input);
			if(Integer.valueOf(option) > 0 && Integer.valueOf(option) <= 3) { }
			else { throw new IOException(); }
		}
		
		while(person == null)
		{
			try
			{
				if(Integer.valueOf(option) == 1) { person =  Customer.newCustomer(input); }
				else if(Integer.valueOf(option) == 2) { person = Employee.newEmployee(input); }
				else if(Integer.valueOf(option) == 3) { person =  Supplier.newSupplier(input); }
				else { person = null; }
			}
			catch(Exception e)
			{
				/* In this block, we handle all of the exceptions we set up based on the message
				 * they brought back to the main program. Each exception message will generate
				 * an appropriate error message before attempting to create the Person again.
				 */
				if(e.getMessage().equals("name"))
				{ System.out.println("Names of persons may not be blank."); }
				
				if(e.getMessage().equals("supplyName"))
				{ System.out.println("Names of supplies may not be blank."); }
				
				if(e.getMessage().equals("purchases"))
				{ System.out.println("Number of purchases must be 1 or greater."); }
				
				if(e.getMessage().equals("hours"))
				{ System.out.println("Employees must work a positive, non-zero number of hours, and no more than " + (24*7) + " hours."); }
				
				if(e.getMessage().equals("payRate"))
				{ System.out.println("No employee may be paid less than $" + Employee.MINIMUM_WAGE + " per hour."); }
				
				if(e.getMessage().equals("otPay"))
				{ System.out.println("All employees must receive at least time-and-a-half, or " + Employee.MINIMUM_OT + " times base pay for overtime."); }
				
				if(e.getMessage().equals("yearStart"))
				{ System.out.println("Contracts may not begin before 1901."); }
				
				if(e.getMessage().equals("yearEnd"))
				{ System.out.println("No contract may end before it begins."); }				
			}
			//break;
		}
		
		return person;
	}
	
	/**
	 * Shows the main menu for the program.
	 */
	private static String showMainMenu(Scanner input)
	{
		System.out.println("Please select an option below:");
		System.out.println("1. Add a person");
		System.out.println("2. Remove the last person");
		System.out.println("3. Review all people");
		System.out.println("Q. Quit this program");
		return input.nextLine();
	}
	
	/**
	 * Shows the relationship menu for new people.
	 */
	private static String personMenu(Scanner input)
	{
		System.out.println("Please select which of the following best describes this person:");
		System.out.println("1. Customer");
		System.out.println("2. Employee");
		System.out.println("3. Supplier");
		return input.nextLine();
	}
	
	/**
	 * Takes in a {@linkplain String} for the prompt, and a {@linkplain Scanner}
	 * for input. Returns input value directly to the calling location in the program.
	 * 
	 * @param	prompt	a {@linkplain String} to prompt the user with
	 * @param	input	a {@linkplain Scanner} to get input from
	 * @return			a {@linkplain String} with the result of input
	 */
	public static String prompt(String prompt, Scanner input)
	{
		System.out.print(prompt);
		return input.nextLine();
	}
	
	/**
	 * Takes in a {@linkplain String} and a char, and compares the first character of
	 * the string to the char. Also takes a {@linkplain Boolean} to determine if this
	 * comparison is case sensitive or not. Note that the char must be a non-uppercase
	 * letter for case-insensitive comparisons.
	 * 
	 * @param string		a {@linkplain String} to check
	 * @param c				a char to check for
	 * @param caseSensitive	a bool to define if the check is case sensitive
	 * @return
	 */
	public static boolean compareFirstCharTo(String string, char c, boolean caseSensitive)
	{
		if(caseSensitive) { return string.charAt(0) == c; }
		return string.toLowerCase().charAt(0) == c;
	}
}
