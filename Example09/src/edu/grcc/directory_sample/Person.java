package edu.grcc.directory_sample;

/**
 * 
 * This class defines the abstract properties of a person, to be implemented in individual
 * classes by type of person.
 * 
 * @author	Nathaniel Knight < nathaniel.d.knight @ gmail.com>
 * @version	1
 * @since	2015-11-03
 * 
 */
public abstract class Person {
	protected String name;
	
	/**
	 * 
	 * @return	the name of this object
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Sets the name value of this object
	 * 
	 * @param	name	the value to assign to name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Abstract string output method; must be supplied by child classes to
	 * be used.
	 * 
	 * @return	a {@linkplain String} identifying the person
	 */
	public abstract String toString();
}
