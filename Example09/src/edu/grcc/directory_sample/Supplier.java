package edu.grcc.directory_sample;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * This class defines the properties of a Supplier for a business. Each supplier has
 * a contract term and supplies a given product. Suppliers can offer only one supply
 * in this definition of a supplier.
 * 
 * As of this pass, the program includes exception handling code to resolve
 * errors, and allow the program to run through certain invalid inputs.
 * 
 * This file should accompany Example09.java
 * 
 * @author	Nathaniel Knight < nathaniel.d.knight @ gmail.com>
 * @version	3
 * @since	2015-11-03
 */
public class Supplier extends Person {
	
	private String supplies;
	private int contractStart, contractEnd;

	/**
	 * Constructs a new Supplier from a name, name of supplies, and the start
	 * and end years of the supplier's contract.
	 * 
	 * @param	name	String name of this Supplier
	 * @param	supplies	String name of supplies provided in this contract
	 * @param	contractStart	Integer year in which the supplier contract begins
	 * @param	contractEnd	Integer year in which the supplier contract ends
	 */
	public Supplier(String name, String supplies, int contractStart, int contractEnd)
	{
		setName(name);
		setSupplies(supplies);
		setContract(contractStart, contractEnd);
	}
	
	/**
	 * Constructs a new Supplier as a copy of an existing Supplier.
	 * 
	 * @param	supplier	Supplier to copy into this Supplier
	 */
	public Supplier(Supplier supplier)
	{
		setName(supplier.getName());
		setSupplies(supplier.getSupplies());
		setContract(supplier.getContractStart(), supplier.getContractEnd());
	}
	
	/**
	 * 
	 * @return	String name of supplies provided by this supplier contract
	 */
	public String getSupplies()
	{ return supplies; }

	/**
	 * Sets the name of the supplies provided by this contract.
	 * 
	 * @param	supplies	String name to assign to supplies
	 */
	public void setSupplies(String supplies)
	{ this.supplies = supplies; }

	/**
	 * 
	 * @return Integer year in which the supplier contract begins
	 */
	public int getContractStart()
	{ return contractStart; }

	/**
	 * Sets the start year of the supplier contract.
	 * 
	 * @param	contractStart	Integer year to set to contractStart
	 */
	private void setContractStart(int contractStart)
	{ this.contractStart = contractStart; }

	/**
	 * 
	 * @return	Integer year in which the supplier contract ends
	 */
	public int getContractEnd()
	{ return contractEnd; }

	/**
	 * Sets the end year of the supplier contract.
	 * 
	 * @param	contractEnd	Integer year to set to contractEnd
	 */
	private void setContractEnd(int contractEnd)
	{ this.contractEnd = contractEnd; }
	
	/**
	 * Sets a contract term from integer years representing the start and
	 * end years for the contract.
	 * 
	 * @param	start	Integer year to set to contractStart
	 * @param	end	Integer year to set to contractEnd
	 */
	public void setContract(int start, int end)
	{
		setContractStart(start);
		setContractEnd(end);
	}

	/**
	 * A static builder method to create a new Suplier with validated input.
	 * 
	 * @param input	->	a Scanner to serve as an input source
	 * @return	->	a validated, well-formed Supplier object
	 * @throws IOException, NumberFormatException	->	Exceptions to be handled elsewhere in the program
	 */
	public static Supplier newSupplier(Scanner input) throws IOException, NumberFormatException
	{
		String name = "";
		String supplies = "";
		int contractStart = 1900;
		int contractEnd = 1900;
		while(name.trim().equals(""))
		{
			name = Example09.prompt("Please enter the name of a supplier: ", input);
			if(name.trim().equals(""))
			{
				throw new IOException("name"); // This exception sends a message "name" indicating a blank name; it will be handled elsewhere
			}
		}
		
		while(supplies.trim().equals(""))
		{
			supplies = Example09.prompt("What supply is this contract for? ", input);
			if(supplies.trim().equals(""))
			{
				throw new IOException("supplyName"); // This exception sends a message "supplyName" indicating a blank name of supplies; it will be handled elsewhere
			}
		}
		
		while(contractStart <= 1900)
		{
			try
			{
				contractStart = Integer.valueOf(Example09.prompt("What year did this contract start? ", input));
			}
			catch(NumberFormatException n) // this block handles invalid numbers, so we look for NumberFormatException
			{
				System.out.println("Must enter a number");
			}
			if(contractStart <= 1900)
			{
				throw new IOException("yearStart"); // This exception sends a message "yearStart" indicating a not allowed start year; it will be handled elsewhere
			}
		}
		
		while(contractEnd < contractStart)
		{
			try
			{
				contractEnd = Integer.valueOf(Example09.prompt("What year will this contract end? ", input));
			}
			catch(NumberFormatException n) // this block handles invalid numbers, so we look for NumberFormatException
			{
				System.out.println("Must enter a number");
			}
			if(contractEnd < contractStart)
			{
				throw new IOException("yearEnd"); // This exception sends a message "yearEnd" indicating a not allowed end year; it will be handled elsewhere
			}
		}
		
		Supplier supplier = new Supplier(name, supplies, contractStart, contractEnd);
		return supplier;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("\n" + getName() + " supplies " + getSupplies());
		sb.append(" until " + getContractEnd() + ".");
		return sb.toString();
	}
}
