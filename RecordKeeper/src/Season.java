public class Season
{
	private int scores[];
	private String seasonName;
	
	public Season(String seasonName, int numberofScores)
	{
		this.seasonName = seasonName;
		for(int i=0;i<numberofScores;i++)
		{
			this.scores[i] = 0;
		}
	}

	public int[] getScores()
	{
		return scores;
	}
	
	public int getScore(int index)
	{
		return scores[index];
	}

	public void setScores(int[] scores) {
		this.scores = scores;
	}
	
	public void setScore(int index, int score)
	{
		this.scores[index] = score;
	}

	public String getSeasonName()
	{
		return seasonName;
	}

	public void setSeasonName(String seasonName)
	{
		this.seasonName = seasonName;
	}
	
	public int getHighScore()
	{
		int score = 0; // the program prohibits scores less than zero
		for(int i = 0; i < scores.length; i++)
		{
			if(scores[i] > score) { score = scores[i]; }
		}
		return score;
	}
	
	public int getLowScore()
	{
		int score = Integer.MAX_VALUE; // any higher score would require a long, so scores in the array should all be below this
		for(int i = 0; i < scores.length; i++)
		{
			if(scores[i] < score) { score = scores[i]; }
		}
		return score;
	}
	
	public double getAverageScore()
	{
		double score = 0d;
		
		for(int i = 0; i < scores.length; i++)
		{
			score += ((double)scores[i]);
		}
		
		return (score/(double)scores.length);
	}
}