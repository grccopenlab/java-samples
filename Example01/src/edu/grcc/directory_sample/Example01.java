package edu.grcc.directory_sample;

import java.util.Scanner;

/**
 * This is the first pass on a business directory project. It is a very simple
 * program which gets input and uses that input to communicate with the user.
 * 
 * @author Nathaniel Knight
 * @version 1
 * @since 2015-09-30
 *
 */
public class Example01
{
	/**
	 * Asks for a name and a relationship, then outputs that name and
	 * relationship to the user.
	 * 
	 * @param	args	unused at present
	 */
	public static void main(final String[] args)
	{
		// first we set up a Scanner which lets us get information from the user
		Scanner input = new Scanner(System.in);
		// then we set up our variables, to store the information
		String name;
		String relationship;
		
		// Here we prompt the user to enter a name...
		System.out.print("Please enter the name of a person: ");
		name = input.nextLine();
		// ... and then a relationship from a list of options
		System.out.print("Is this person a customer, employee, or supplier? ");
		relationship = input.nextLine();
		// we do not check for errors in this version of the program;
		// we will add error-checks in a later version on the project
		
		// a blank line helps to clearly separate the Input and Output portions
		// of a program in the console
		System.out.println("");
		
		// here we output the name and relationship so the user can see the work
		// being done by the program
		System.out.println(name + " is one of your " + relationship + "s.");
		
		// Before our program ends, we need to make sure that our Scanner gets
		// closed, to guard against a memory leak.
		input.close();
	}
}
