package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This class defines the attributes of an Employee for our directory
 * program. This file should accompany Example06.java
 * 
 * @author Nathaniel Knight <nathaniel.d.knight @ gmail.com>
 * @version 1
 * @since 2015-11-03
 * @modified 2016-01-14
 * 
 * Modified 2016 Jan 14: MINIMUM_WAGE updated to reflect Michigan Minimum Wage
 * 
 */
public class Employee extends Person {
	private static final int FULLTIME_HOURS = 40;
	private static final double MINIMUM_WAGE = 8.50;
	private int hours;
	private double payRate;
	private boolean fullTime;

	/**
	 * Constructs a new Employee from a name, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	name	String name for this Customer
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(String name, int hours, double payRate)
	{
		super(name, "Employee");
		
	}

	/**
	 * Constructs new Employee from a Person, hours worked, and wages or
	 * rate of pay.
	 * 
	 * @param	person	Person to base this Employee on
	 * @param	hours	Integer hours worked per week
	 * @param	payRate	Double dollar value of pay per hour worked
	 */
	public Employee(Person person, int hours, double payRate) {
		super(person);
		setHours(hours);
		setPayRate(payRate);
	}
	
	/**
	 * Constructs a new Employee as a copy of an existing Employee.
	 * 
	 * @param	employee	Employee to copy into this Employee
	 */
	public Employee(Employee employee)
	{
		super(employee.getName(), employee.getRelationship());
		setHours(employee.getHours());
		setPayRate(employee.getPayRate());
	}

	/**
	 * 
	 * @return	Integer hours worked per week
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * Sets the number of hours worked per week.
	 * 
	 * @param	hours	Integer to set to hours
	 */
	public void setHours(int hours) {
		this.hours = hours;
		setFullTime();
	}

	/**
	 * 
	 * @return	Double dollar value of pay per hour worked
	 */
	public double getPayRate()
	{
		return payRate;
	}

	/**
	 * Sets the wages or rate of pay per hour.
	 * 
	 * @param	payRate	Double dollar value to set to payRate
	 */
	public void setPayRate(double payRate)
	{
		this.payRate = payRate;
	}

	/**
	 * Compares the current value of hours to the minimum value required for
	 * full-time status, and sets the fullTime value by the result.
	 */
	private void setFullTime()
	{
		if(hours >= FULLTIME_HOURS) { fullTime = true; }
		else { fullTime = false; }
	}

	/**
	 * 
	 * @return	whether this Employee qualifies as full-time
	 */
	public boolean isFullTime()
	{
		return fullTime;
	}
	
	/**
	 * A static builder method to create a new Employee with validated input.
	 * 
	 * @param input	a Scanner to serve as an input source
	 * @return	a validated, well-formed Employee object
	 */
	public static Employee newEmployee(Scanner input)
	{
		String name;
		int hours;
		double payRate;
		boolean valid = false;
		
		do
		{
			name = Example06.prompt("Please enter the name of an employee: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			hours = Integer.valueOf(Example06.prompt("How many hours does this employee work per week? ", input));
			if(hours >= 0)
			{
				valid = true;
			}
			else
			{
				System.out.println("Cannot work negative hours.");
			}
		} while (!valid);
		
		valid = false;
		
		do
		{
			payRate = Double.valueOf(Example06.prompt("how much is this employee paid per hour? ", input));
			if(payRate >= MINIMUM_WAGE)
			{
				valid = true;
			}
			else
			{
				System.out.println("Employees must be paid minimum wage.");
			}
		} while (!valid);
		
		Person person = new Person(name, "Employee");
		Employee employee = new Employee(person, hours, payRate);
		return employee;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append("\n" + getName() + " is ");
		if(!isFullTime())
		{ sb.append("not "); }
		sb.append("a full-time employee.");
		return sb.toString();
	}
}
