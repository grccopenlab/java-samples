package edu.grcc.directory_sample;
import java.util.Scanner;

/**
 * This class defines the attributes of a Supplier for our directory program.
 * This file should accompany Example06.java
 * 
 * @author Nathaniel Knight < nathaniel.d.knight @ gmail.com>
 * @version 1
 * @since 2015-11-03
 * @modified 2015-11-03
 * 
 */
public class Supplier extends Person {
	private String supplies;
	private int contractStart, contractEnd;

	/**
	 * Constructs a new Supplier from a name, name of supplies, and the start
	 * and end years of the supplier's contract.
	 * 
	 * @param	name	String name of this Supplier
	 * @param	supplies	String name of supplies provided in this contract
	 * @param	contractStart	Integer year in which the supplier contract begins
	 * @param	contractEnd	Integer year in which the supplier contract ends
	 */
	public Supplier(String name, String supplies, int contractStart, int contractEnd)
	{
		super(name, "Supplier");
		setSupplies(supplies);
		setContract(contractStart, contractEnd);
	}

	/**
	 * Constructs a new Supplier from a Person, name of supplies, and the start
	 * and end years of the supplier's contract.
	 * 
	 * @param	person	Person to base this Supplier on
	 * @param	supplies	String name of supplies provided in this contract
	 * @param	contractStart	Integer year in which the supplier contract begins
	 * @param	contractEnd	Integer year in which the supplier contract ends
	 */
	public Supplier(Person person, String supplies, int contractStart, int contractEnd)
	{
		super(person);
		setSupplies(supplies);
		setContract(contractStart, contractEnd);
	}
	
	/**
	 * Constructs a new Supplier as a copy of an existing Supplier.
	 * 
	 * @param	supplier	Supplier to copy into this Supplier
	 */
	public Supplier(Supplier supplier)
	{
		super(supplier.getName(), supplier.getRelationship());
		setSupplies(supplier.getSupplies());
		setContract(supplier.getContractStart(), supplier.getContractEnd());
	}
	
	/**
	 * 
	 * @return	String name of supplies provided by this supplier contract
	 */
	public String getSupplies() {
		return supplies;
	}

	/**
	 * Sets the name of the supplies provided by this contract.
	 * 
	 * @param	supplies	String name to assign to supplies
	 */
	public void setSupplies(String supplies) {
		this.supplies = supplies;
	}

	/**
	 * 
	 * @return Integer year in which the supplier contract begins
	 */
	public int getContractStart() {
		return contractStart;
	}

	/**
	 * Sets the start year of the supplier contract.
	 * 
	 * @param	contractStart	Integer year to set to contractStart
	 */
	private void setContractStart(int contractStart) {
		this.contractStart = contractStart;
	}

	/**
	 * 
	 * @return	Integer year in which the supplier contract ends
	 */
	public int getContractEnd() {
		return contractEnd;
	}

	/**
	 * Sets the end year of the supplier contract.
	 * 
	 * @param	contractEnd	Integer year to set to contractEnd
	 */
	private void setContractEnd(int contractEnd) {
		this.contractEnd = contractEnd;
	}
	
	/**
	 * Sets a contract term from integer years representing the start and
	 * end years for the contract.
	 * 
	 * @param	start	Integer year to set to contractStart
	 * @param	end	Integer year to set to contractEnd
	 */
	public void setContract(int start, int end)
	{
		setContractStart(start);
		setContractEnd(end);
	}

	/**
	 * A static builder method to create a new Suplier with validated input.
	 * 
	 * @param input	a Scanner to serve as an input source
	 * @return	a validated, well-formed Supplier object
	 */
	public static Supplier newSupplier(Scanner input)
	{
		String name;
		String supplies;
		int contractStart, contractEnd;
		boolean valid = false;
		
		do
		{
			name = Example06.prompt("Please enter the name of a supplier: ", input);
			if(name.equals(""))
			{
				System.out.println("Invalid name.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		do
		{
			supplies = Example06.prompt("What is this supply contract for? ", input);
			if(supplies.equals(""))
			{
				System.out.println("Must supply something.");
			}
			else
			{ valid = true; }
		} while (!valid);
		
		valid = false;
		
		contractStart = Integer.valueOf(Example06.prompt("What year did this contract start? ", input));
		do
		{
			contractEnd = Integer.valueOf(Example06.prompt("What year will this contract end? ", input));
			if(contractEnd < contractStart)
			{ System.out.println("Contracts cannot end before they begin." ); }
			else
			{ valid = true; }
		} while (!valid);
		
		Person person = new Person(name, "Supplier");
		Supplier supplier = new Supplier(person, supplies, contractStart, contractEnd);
		return supplier;
	}
	
	/**
	 * Outputs the identity of this instance as a String.
	 * 
	 * @return	a {@linkplain String} identifying the person and their relationship
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append("\n" + getName() + " supplies " + getSupplies());
		sb.append(" until " + getContractEnd() + ".");
		return sb.toString();
	}

}
