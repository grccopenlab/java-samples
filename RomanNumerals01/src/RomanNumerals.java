import java.util.Scanner;

public class RomanNumerals
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int number;
		String romanNumeral;
		
		System.out.println("Welcome to the roman numeral converter.");
		System.out.print("Please enter a number to convert to roman numerals: ");
		number = Integer.valueOf(input.nextInt());
		
		romanNumeral = intToRoman(number);
		
		System.out.println("The number " + number + " in roman numerals is: " + romanNumeral);
		input.close();
	}
	
	/**
	 * 
	 * @param number = the number to be converted
	 *
	 * @return returns the string representing number as a Roman numeral
	 *
	 */
	public static String intToRoman(int number)
	{
		String temp = "";
		
		while(number >= 1000)
		{
			temp = temp + "M"; 
			number = number - 1000;
		}
		
		while(number >= 500)
		{
			temp = temp + "D";
			number = number - 500;
		}
		
		while(number >= 100)
		{
			temp = temp + "C";
			number = number - 100;
		}
		
		while(number >= 50)
		{
			temp = temp + "L";
			number = number - 50;
		}
		
		while(number >= 10)
		{
			temp = temp + "X";
			number = number - 10;
		}
		
		while(number >= 5)
		{
			temp = temp + "V";
			number = number - 5;
		}
		
		while(number >= 1)
		{
			temp = temp + "I";
			number = number - 1;
		}
			
		return temp;
	}
	
}
