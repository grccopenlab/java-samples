import java.util.Scanner; // This package is required for input through Scanner

// This is the prototype CalorieCounter program -- it will not use any
// methods except main() to achieve its goal of calculating how many
// calories a user has eaten over the course of one day with 4 meals.
// Any meal skipped can be entered as 0 for all values.

public class CalorieCounter {

	public static void main(String args[]) {
		// This is where we declare all of our variables for the program.
		String userName;
		// our other variables will be named unitNutrientMeal for grams
		// and unitsNutrient for calories.
		int gramsFatBreakfast, gramsProteinBreakfast, gramsCarbohydratesBreakfast;
		int gramsFatLunch, gramsProteinLunch, gramsCarbohydratesLunch;
		int gramsFatSupper, gramsProteinSupper, gramsCarbohydratesSupper;
		int gramsFatSnack, gramsProteinSnack, gramsCarbohydratesSnack;
		int caloriesFat, caloriesProtein, caloriesCarbohydrates;
		// This is required to get input
		Scanner input = new Scanner(System.in);

		// This is where we will begin our input collection code.
		System.out.print("Please enter your name: ");
		userName = input.nextLine();
		
		// Here is a text about the program:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("This program is designed to take your eating habits and calculate the number ");
		System.out.println("of calories you took in for a day. Please enter the numbers below in grams, "); 
		System.out.println("not ounces, eaten at the listed meal. If you did not have the listed nutrient ");
		System.out.println("at the listed meal, enter 0. If you did not eat the listed meal, enter 0 for ");
		System.out.println("all nutrients.");
		System.out.println(""); // this blank line provides padding for the user between sections
		// End of informational text
		
		// Here we get per-meal input. Each meal will start with a blank
		// line and a prompt for the meal name.
		
		// Breakfast
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Information for Breakfast");
		System.out.println(userName + " please enter the grams of fat you had with breakfast: ");
		gramsFatBreakfast = input.nextInt();
		System.out.println(userName + " please enter the grams of protein you had with breakfast: ");
		gramsProteinBreakfast = input.nextInt();
		System.out.println(userName + " please enter the grams of carbohydrates you had with breakfast: ");
		gramsCarbohydratesBreakfast = input.nextInt();
		
		// Lunch
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Information for Lunch");
		System.out.println(userName + " please enter the grams of fat you had with lunch: ");
		gramsFatLunch = input.nextInt();
		System.out.println(userName + " please enter the grams of protein you had with lunch: ");
		gramsProteinLunch = input.nextInt();
		System.out.println(userName + " please enter the grams of carbohydrates you had with lunch: ");
		gramsCarbohydratesLunch = input.nextInt();
		
		// Supper
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Information for Supper");
		System.out.println(userName + " please enter the grams of fat you had with supper: ");
		gramsFatSupper = input.nextInt();
		System.out.println(userName + " please enter the grams of protein you had with supper: ");
		gramsProteinSupper = input.nextInt();
		System.out.println(userName + " please enter the grams of carbohydrates you had with supper: ");
		gramsCarbohydratesSupper = input.nextInt();

		// Snack
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Information for Snacks");
		System.out.println(userName + " please enter the grams of fat you had while snacking: ");
		gramsFatSnack = input.nextInt();
		System.out.println(userName + " please enter the grams of protein you had while snacking: ");
		gramsProteinSnack = input.nextInt();
		System.out.println(userName + " please enter the grams of carbohydrates you had while snacking: ");
		gramsCarbohydratesSnack = input.nextInt();
		// End of Meal Data Collection
		// End of Data Collection
		
		// In this section, we perform our calculations using the data entered above:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Calculating fat calories...");
		caloriesFat = (gramsFatBreakfast + gramsFatLunch + gramsFatSupper + gramsFatSnack) * 9;
		System.out.println("Calculating protein calories...");
		caloriesProtein = (gramsProteinBreakfast + gramsProteinLunch + gramsProteinSupper + gramsProteinSnack) * 9;
		System.out.println("Calculating carbohydrate calories...");
		caloriesCarbohydrates = (gramsCarbohydratesBreakfast + gramsCarbohydratesLunch + gramsCarbohydratesSupper + gramsCarbohydratesSnack) * 9;
		System.out.println("Calculations complete.");
		
		// In this section, we use our calculated values to output totals:
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Your calorie report is ready, " + userName + "!");
		System.out.println("You had " + (gramsFatBreakfast + gramsFatLunch + gramsFatSupper + gramsFatSnack) + "g of fat, for a total of " + caloriesFat + " calories from fat.");
		System.out.println("You had " + (gramsProteinBreakfast + gramsProteinLunch + gramsProteinSupper + gramsProteinSnack) + "g of protein, for a total of " + caloriesProtein + " calories from protein.");
		System.out.println("You had " + (gramsCarbohydratesBreakfast + gramsCarbohydratesLunch + gramsCarbohydratesSupper + gramsCarbohydratesSnack) + "g of carbohydrates, for a total of " + caloriesCarbohydrates + " calories from carbohydrates.");
		
		System.out.println(""); // this blank line provides padding for the user between sections
		System.out.println("Thank you for using the calorie calculator.");
		
		input.close();		
	}

}
